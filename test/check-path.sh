#!/bin/bash
# Test DCube is installed and runs from the various paths it has been available from at one time or another.
# If everything is setup consistently, should list the same file and print the same version number 4 times.

# Require PyROOT is available to test running
if python -c 'import ROOT' &> /dev/null; then
  rundcube=""
else
  rundcube=":"
  echo "=== PyROOT not available, so can't run dcube.py"
fi

if [ -z "$ATLAS_LOCAL_ROOT" ]; then
  echo "=== \$ATLAS_LOCAL_ROOT is not defined"
else
  echo "=== check recommended path under \$ATLAS_LOCAL_ROOT"
  ls -lL    "$ATLAS_LOCAL_ROOT/dcube/current/DCubeClient/python/dcube.py"
  $rundcube "$ATLAS_LOCAL_ROOT/dcube/current/DCubeClient/python/dcube.py" --version
fi

if [  ! -d "/cvmfs/atlas.cern.ch/repo" ]; then
  echo "=== /cvmfs/atlas.cern.ch/repo is not available"
else
  echo "=== check old recommended path (25/9/2019 - 9/4/2020)"
  ls -lL    "/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/dcube/current/DCubeClient/python/dcube.py"
  $rundcube "/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/dcube/current/DCubeClient/python/dcube.py" --version

  echo "=== check old recommended path (23/3/2018 - 25/9/2019)"
  ls -lL    "/cvmfs/atlas.cern.ch/repo/sw/art/dcube/DCubeClient/python/dcube.py"
  $rundcube "/cvmfs/atlas.cern.ch/repo/sw/art/dcube/DCubeClient/python/dcube.py" --version
fi

if [ -z $(type -t "dcube.py") ]; then
  echo "=== dcube.py is not in \$PATH"
else
  echo "=== check dcube.py from \$PATH"
  ls -lL "$(type -p "dcube.py")"
  $rundcube         "dcube.py" --version
fi
