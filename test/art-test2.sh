#!/bin/bash
# Compare InDetSLHC_Example 10 GeV muons against 100 GeV muons using art-dcube wrapper.
# This produces many plots, some with warnings (eg. empty).
# We expect some interesting differences, so will get comparison failures.

dir=$(dirname "$0")
name=$(basename "$0" ".sh")
dcubedir=$(dirname "$dir")
reldir=$(basename "$dir")
tmp="${TMPDIR:-/tmp}/dcube-$name.$$"
rootver="views LCG_96b x86_64-centos7-gcc8-opt"
keep=0 atlas=0 show=:
while [ $# -gt 0 ]; do  # must come before dcube.py options
  case "$1" in
    -k) keep=1; shift;;
    -v) show=""; shift;;
    -a) atlas=1; shift;;
    -r) rootver="$2"; shift; shift;;
    -c) tmp=$(pwd)/"$name"; keep=1; shift;;
    *) break;;
  esac
done

function tidyxml()
{
  sed -e 's=plots/[^"]*\.png=plots/X.png=g' -e '{$!{N;N;s=<date>\s*.*\s*</date>==;t;P;D}}' "$@"
}

function tidylog()
{
  sed -e 's= -x [^ ]*/art-test. = =' "$@"
}

function tidyhtml()
{
  sed -E \
    -e 's%plots/(g[rd]|[ph][12][rdxy])_[0-9a-f-]{36}\.png%X.png%g' \
    -e "s%<span class='bold'>20[0-9]{2}-[0-9]{2}-[0-9]{2}</span>%%g" \
    -e 's%^  \* on .*$%%' \
    -e 's%^  \* \[3\] config XML file = .*$%%' \
    -e 's% DCube v?[0-9]\.[0-9](\.[0-9])?% DCube %g' \
    -e 's%<b>[0-9]\.[0-9](\.[0-9])?</b> DCube % DCube %g' \
    -e 's% +$%%' \
    "$@"
}

if [ $atlas -ne 0 ]; then
  test -n "$ATLAS_DCUBE_DIR" && dcubedir="$ATLAS_DCUBE_DIR/DCubeClient";
else
  # Set up ROOT. Version 6.18 has a different XML formatting (vs newer 6.20), so we simplify the comparison by requiring this version here.
  unset PYTHONHOME PYTHONPATH  # remove any previous Python environment
  export ATLAS_LOCAL_ROOT_BASE="/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase"
  $show echo + \
  . "$ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh" -q >&2
  . "$ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh" -q
  $show echo + \
  . "$ATLAS_LOCAL_ROOT_BASE/packageSetups/localSetup.sh" -q "$rootver" >&2
  . "$ATLAS_LOCAL_ROOT_BASE/packageSetups/localSetup.sh" -q "$rootver"
fi

$show set -x
# Don't include current asetup in result xml.
unset AtlasBuildBranch CMTCONFIG AtlasReleaseType AtlasVersion AtlasProject
# cd to dcube directory so paths in log are the same as in the reference
cd "$dcubedir"

mkdir -p "$tmp"
# The config file was copied from /cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/InDetSLHC_Example/dcube/config/ITk_IDPVM.xml .
"./bin/art-dcube" \
  "InclinedAlternative" \
  "/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/InDetSLHC_Example/ReferenceHistograms/physval.ATLAS-P2-ITK-17-04-02_single_mu_Pt10_digi.root" \
  "./$reldir/${name}-config.xml" \
  "/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/InDetSLHC_Example/ReferenceHistograms/physval.ATLAS-P2-ITK-17-04-02_single_mu_Pt100_digi.root" \
  "$tmp/$name" \
  "$@" \
  &> "$tmp/$name.log"
$show cat "$tmp/$name.log"

test -s "$tmp/$name.log"        || exit 97
diff <(tidylog "$dir/$name.log") <(tidylog "$tmp/$name.log")
stat=$?
test -s "$tmp/$name/dcube.xml"  || exit 98
diff <(tidyxml "$dir/${name}-result.xml") <(tidyxml "$tmp/$name/dcube.xml") || \
stat=$?
test -s "$tmp/$name/dcube.html" || exit 99
diff <(tidyhtml "$dir/${name}.html") <(tidyhtml "$tmp/$name/dcube.html") || \
stat=$?
# $show ls -lR "$tmp"   # reduce logfile size
set +x

if [ $keep -eq 0 ]; then
  rm -rf "$tmp"
  $show echo "$(basename "$0") exit status $stat" >&2
else
  echo "$(basename "$0") exit status $stat - output saved in $tmp" >&2
fi
exit $stat
