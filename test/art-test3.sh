#!/bin/bash
# Compare InDetPhysValMonitoring LRT R24 vs R23 for ttbar with pileup mu=40 using dcube.py.
dir=$(dirname "$0")
name=$(basename "$0" ".sh")
dcubedir=$(dirname "$dir")
tmp="${TMPDIR:-/tmp}/dcube-$name.$$"
rootver="views LCG_106 x86_64-el9-gcc13-opt"
keep=0 atlas=0 show=:
while [ $# -gt 0 ]; do  # must come before dcube.py options
  case "$1" in
    -k) keep=1; shift;;
    -v) show=""; shift;;
    -a) atlas=1; shift;;
    -r) rootver="$2"; shift; shift;;
    -c) tmp=$(pwd)/"$name"; keep=1; shift;;
    *) break;;
  esac
done

function tidyxml()
{
  sed -e 's=plots/[^"]*\.png=plots/X.png=g' -e '{$!{N;N;s=<date>\s*.*\s*</date>==;t;P;D}}' "$@"
}

function tidylog()
{
  sed -e 's= -x [^ ]*/art-test. = =' "$@"
}

function tidyhtml()
{
  sed -E \
    -e 's%plots/(g[rd]|[ph][12][rdxy])_[0-9a-f-]{36}\.png%X.png%g' \
    -e "s%<span class='bold'>20[0-9]{2}-[0-9]{2}-[0-9]{2}</span>%%g" \
    -e 's%^  \* on .*$%%' \
    -e 's%^  \* \[3\] config XML file = .*$%%' \
    -e 's% DCube v?[0-9]\.[0-9](\.[0-9])?% DCube %g' \
    -e 's%<b>[0-9]\.[0-9](\.[0-9])?</b> DCube % DCube %g' \
    -e 's% +$%%' \
    "$@"
}

if [ $atlas -ne 0 ]; then
  test -n "$ATLAS_DCUBE_DIR" && dcubedir="$ATLAS_DCUBE_DIR/DCubeClient";
else
  # Set up ROOT.
  unset PYTHONHOME PYTHONPATH  # remove any previous Python environment
  export ATLAS_LOCAL_ROOT_BASE="/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase"
  $show echo + \
  . "$ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh" -q >&2
  . "$ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh" -q
  $show echo + \
  . "$ATLAS_LOCAL_ROOT_BASE/packageSetups/localSetup.sh" -q "$rootver" >&2
  . "$ATLAS_LOCAL_ROOT_BASE/packageSetups/localSetup.sh" -q "$rootver"
fi

$show set -x
mkdir -p "$tmp"
"$dcubedir/python/dcube.py" -p \
   --branch="*" --cmtconfig="*" --install="*" --jobId="*" --project="*" \
   -x "$tmp/$name" \
   -c "$dir/${name}-config.xml" \
   -r "/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/InDetPhysValMonitoring/ReferenceHistograms/physval_run4_ttbar0PU_reco_r25.root" \
   "$@" \
      "/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/InDetPhysValMonitoring/ReferenceHistograms/physval_run4_ttbar200_reco_r25.root" \
   &> "$tmp/$name.log"
$show cat "$tmp/$name.log"

test -s "$tmp/$name.log"        || exit 97
diff <(tidylog "$dir/$name.log") <(tidylog "$tmp/$name.log")
stat=$?
test -s "$tmp/$name/dcube.xml"  || exit 98
diff <(tidyxml "$dir/${name}-result.xml") <(tidyxml "$tmp/$name/dcube.xml") || \
stat=$?
test -s "$tmp/$name/dcube.html" || exit 99
diff <(tidyhtml "$dir/${name}.html") <(tidyhtml "$tmp/$name/dcube.html") || \
stat=$?
$show ls -lR "$tmp"
set +x

if [ $keep -eq 0 ]; then
  rm -rf "$tmp"
  $show echo "$(basename "$0") exit status $stat" >&2
else
  echo "$(basename "$0") exit status $stat - output saved in $tmp" >&2
fi
exit $stat
