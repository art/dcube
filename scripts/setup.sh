# enviroment setup script for dcube.py running for (ba,z)sh shell
#
# Aug 16, 2007 - cibak - switch to root 5.16/00
# Oct 23, 2007 - cibak - switch to root 5.17/04
# Apr 10, 2008 - cibak - switch to root 5.19/02 and python 2.5
# May 27, 2020 - adye - ALRB setup. Don't add ROOT, so can keep user's choice.

if [ -z "$ATLAS_DCUBE_DIR" ]; then
  atlas_dcube_src=${BASH_ARGV[0]}
  test "x$atlas_dcube_src" = "x" && atlas_dcube_src=${(%):-%N} # for zsh
  if [ "x$atlas_dcube_src" != "x" ]; then
    atlas_dcube_src="`dirname \"$atlas_dcube_src\"`"
    ATLAS_DCUBE_DIR="`cd \"$atlas_dcube_src/..\" > /dev/null; pwd`"
    test "x$ATLAS_DCUBE_DIR" = "x" && ATLAS_DCUBE_DIR="/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/dcube/current"
  fi
  unset atlas_dcube_src
  export ATLAS_DCUBE_DIR
fi
PATH="$ATLAS_DCUBE_DIR/bin:$PATH"
export PATH
