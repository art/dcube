#!/bin/bash
# Create tarball for installation in ALRB

package="dcube"
repo="https://gitlab.cern.ch/art/dcube.git"

if [ $# -ne 1 -o "$1" = "-h" -o "$1" = "--help" ]; then
  echo "Usage: $(basename "$0") TAG" >&2
  exit 1
fi

tag="$1"
cwd="$PWD"
outfile="$PWD/${package}-$tag.tar.gz"
tmproot="${TMPDIR:-/tmp}/${package}-install.$$"

function test_dcube()
{
  (
    echo "+ asetup $@" >&2
    export ATLAS_LOCAL_ROOT_BASE="/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase"
    . "$ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh" -q
    . "$AtlasSetup/scripts/asetup.sh" "$@"
    rm -f .asetup.save CMakeLists.txt
    set -x
    ./bin/dcube.py --version
  )
}

(
  set -x
  mkdir -p "$tmproot" "$tmproot/$package"
  cd "$tmproot"
  git clone -c advice.detachedHead=false -b "$tag" "$repo" "${package}-git"
  cd "${package}-git"
  git archive --format=tar "$tag" | tar -C "../$package" -xf -
  cd "../$package"

  # Install DCube by creating symlinks to find files in the expected places.
  scripts/install.sh
)

cd "$tmproot/$package"

# Run dcube.py --version for Python 3.9 and Python 3.11 to make .pyc files.
test_dcube Athena,24.0,latest
test_dcube Athena,main,latest

(
  set -x
  ls -1A | xargs -d "\n" tar -zcf "$outfile"
  cd "$cwd"
  rm -rf "$tmproot"
)
