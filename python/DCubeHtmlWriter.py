#!/bin/env python

# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
##
# @file DCubeClient/python/DCubeHtmlWriter.py
# @author Tim Adye
# @brief implementation of DCubeHtmlWriter and DCubeLogWriter classes
# This file is loosly based on share/DCubeServer/dcube.php (Krzyszotf Daniel Ciba, 2023-04-03),
# initially converted with https://extendsclass.com/php-to-python.html,
# which uses https://github.com/nicolasrod/php2python

from __future__ import print_function
import sys, re, os.path
import xml.dom.minidom
import xml.parsers.expat
from time import strftime, localtime
import base64
import json

from .DCubeUtils import DCubeObject, DCubeVersion
from .DCubeUtils import share_path

try:
    from html import escape  # Python 3.2+
except ImportError:
    from cgi import escape  # will eventually be depreciated

# Convenience routines to help with PHP->Python migration
echo = None  # define with closure to write to output HTML


def num(a, d="&mdash;"):
    return (
        re.sub(r"\.$", "", re.sub(r"(\.\d*?)0*$", r"\1", "%f" % float(a)))
        if a is not None and a != ""
        else d
    )


##
# @class DCubeHtmlWriter
# @author Krzysztof Daniel Ciba (Krzytof.Ciba@NOSPAMgmail.com)
# @brief A class for displaying body part of DCubeServer HTML page.
class DCubeHtmlWriter(DCubeObject):
    opts = None
    args = None

    # DCubeVersion object
    version = None
    # path (absolute or relative) to CSS file
    css = "css/dcube.css"
    # path (absoulte or relative to JS file
    js = "dcube.js"

    # XML input file
    xml_file = "dcube.xml"
    # log file to link to
    log_file = "dcube.log"
    # HTML output file
    html_file = "dcube.html"
    # directory containing html_file
    outdir = "."

    # input xml data (xml.dom.minidom format)
    xmldoc = None
    # parsed xml data (xml.parsers.expat format)
    xml = None
    # default p-value limit for warning
    pwarn = 0.95
    # default p-value limit for failed tests
    pfail = 0.7
    # counter for histograms
    id = 0
    # JS DOM table for img src
    picTableJS = ""
    # JS DOM table for img info
    picInfoTableJS = ""
    picNum = 0

    srcTable = {}

    ## c'tor
    # @param self "My, myself and Irene"
    # @param parsed opts and args from DCubeOptParser
    def __init__(
        self,
        xmldoc=None,
        parsed=(None, []),
        xml_file=None,
        log_file=None,
        html_file=None,
    ):
        super(DCubeHtmlWriter, self).__init__(self)

        self.version = DCubeVersion()
        self.xmldoc = xmldoc
        self.opts, self.args = parsed

        if xml_file:
            self.xml_file = xml_file
        elif self.opts and self.opts.output:
            self.xml_file = self.opts.output

        if log_file:
            self.log_file = log_file
        elif self.opts and self.opts.log:
            self.log_file = self.opts.log

        if html_file:
            self.html_file = html_file
            self.outdir = os.path.dirname(html_file)
        else:
            html_file, nsub = re.subn(r"([^./])\.[^./]+$", r"\1.html", self.xml_file)
            if nsub:
                self.html_file = html_file
                self.outdir = os.path.dirname(html_file)

        self.log_file = self.relpath(self.log_file)

        self.loading_src = self.imgToUri("images/loading.svg")

    def dcubeHTML(self):
        if self.xmldoc:
            self.xml = DCubeXmlParser().Parse(self.xmldoc.toxml())
        else:
            if not self.xml_file:
                self.warn("no HTML output file specified, so none written")
                return
            # parse xml file
            fp = open(self.xml_file, "rb")
            if not fp:
                self.error("Cannot open XML file" + self.xml_file)
                return
            self.xml = DCubeXmlParser().ParseFile(fp)
            fp.close()

        if self.html_file:
            html_fp = open(self.html_file, "w")
        else:
            html_fp = None  # -> sys.stdout

        def echo_html(*args):
            print(*args, end="", file=html_fp)

        global echo
        echo = echo_html  # make global function for all of DCubeHtmlWriter.py

        echo(
            '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">\n\n'
        )
        echo("<html>")
        self.__header()
        self.__comment()
        self.__body()
        echo("</html>")

        echo = None
        html_fp = None  # close file

    # @brief This method produces HTML header for DCubeServer page.
    def __header(self):
        branch = self.xml["dcube"][0]["attr"].get("branch")
        install = self.xml["dcube"][0]["attr"].get("install")
        jobId = self.xml["dcube"][0]["attr"].get("jobid")

        echo("\n<head>\n")
        echo(
            "<title>"
            + " ".join([t for t in [jobId, install, branch] if t and t != "*"])
            + " - "
            + self.version.project()
            + " v"
            + self.version.versionNumber()
            + " </title>\n"
        )
        echo("<meta http-equiv='content-type' content='text/html; charset=utf-8'>\n")
        echo("<meta http-equiv='content-language' content='en'>\n")
        echo("<meta name='referrer' content='unsafe-url'>")
        echo("<meta name ='description' content='DCubeServer page'>\n")
        echo(
            "<meta name='keywords' content='webbased root histogram displaying ATLAS CERN DCube'>\n"
        )
        echo("<meta name='author' content='" + self.version.authorName() + "'>\n")

        echo("<style>\n")
        with share_path(os.path.join("DCubeServer", self.css)) as path:
            with open(path) as f:
                echo(f.read())
        echo("\n</style>\n")

        favicon_uri = self.imgToUri("images/favicon.gif")
        echo("<link rel='shortcut icon' href='" + favicon_uri + "' type='image/gif'>\n")
        echo("<link rel='icon' type='image/gif' href='" + favicon_uri + "'>\n")
        echo("<script>\n")
        with share_path(os.path.join("DCubeServer", self.js)) as path:
            with open(path) as f:
                echo(f.read())
        echo("\n</script>\n")

        echo("</head>\n")

    ## html comment string
    # @param self "Me, myself and Irene"
    def __comment(self, what="output"):
        echo("<!--*****************************************************\n")
        echo("  * DCubeClient HTML %s file\n" % what)
        echo("  * autogenerated using %s\n" % self.version.version())
        echo("  * on %s\n" % strftime("%a, %d %b %Y %H:%M:%S %Z", localtime()))
        if self.opts:
            echo("  * Files:\n")
            echo("  * [1] monitored file  = %s\n" % self.relpath(self.opts.monitored))
            echo("  * [2] reference file  = %s\n" % self.relpath(self.opts.reference))
            echo("  * [3] config XML file = %s\n" % self.relpath(self.opts.config))
            echo("  * [4] output XML file = %s\n" % self.relpath(self.xml_file))
            echo("  * [5] log file        = %s\n" % self.log_file)
        echo("  * *****************************************************-->\n")

    def __body(self):
        # take values for prob limits
        self.pwarn = self.xml["dcube"][0]["plimit"][0]["attr"].get("warn")
        # set to default if absent
        if self.pwarn == 0:
            self.pwarn = 0.95
        self.pfail = self.xml["dcube"][0]["plimit"][0]["attr"].get("fail")
        # set to deafult if absent
        if self.pfail == 0:
            self.pfail = 0.75
        echo("<body onload='jsfix();'>\n")
        echo("<script type='text/javascript'><!--\n")
        echo(
            "document.write('%s');\n"
            % r'<div id="loading"><br\/>Please wait...<div id="load">&nbsp;<\/div><\/div>'
        )
        echo("--></script>\n")
        echo("<div class='wrapper'>\n")
        if "php" in self.opts.webDisplay:
            echo(
                "<div class='lheader'><a href='dcube.xml.php'>[ Go to PHP display ]</a></div>\n"
            )
        echo(
            "<div class='header'><b>"
            + self.version.versionNumber()
            + "</b> "
            + self.version.project()
            + " by "
            + "<a href='mailto:"
            + self.version.authorEmail()
            + "?subject="
            + self.version.project().lower()
            + "'>"
            + self.version.authorName()
            + "</a></div>\n"
        )
        echo("<div class='container'>\n")
        # configuration panel
        echo("<div class='mid_panel'>")
        self.configurationPanel()
        echo("</div>\n")
        # static legend display
        echo("<div class='legend' title='Show/hide legend window'>")
        self.staticLegend()
        echo("</div>")
        # main panel content
        echo("<div class='content'>\n")
        self.mainPanel()
        echo("</div>\n")
        # end of container
        echo("</div>\n")
        # footer content
        echo("<div class='footer'>")
        self.footer()
        echo("</div>\n")
        # end of wrapper
        echo("</div>\n")
        # display statistics tests legend
        self.testLegend()
        # moving out of body
        echo("<script type='text/javascript'>\n")
        echo("<!--\n")
        echo("function jsfix() {\n")
        echo("document.getElementById('loading').style.display = 'none';\n")
        echo("}\n")
        echo("var loading = new Image();\n")
        echo("loading.src = '" + self.loading_src + "';\n")
        echo("var picTable = new Array();\n")
        echo("var picInfoTable = new Array();\n")

        echo("var srcTable = " + json.dumps(self.srcTable, indent=2))
        echo("\n")
        echo(
            """
function getPlot(key) {
    return srcTable[key];
}
             """.strip()
        )
        echo("\n")

        echo(self.picTableJS)
        echo(self.picInfoTableJS)
        echo("setJumpList();\n")
        echo("gGetBeg();\n")
        echo("setFirstPane();\n")
        echo("optNumPicToShow();\n")
        # allow for picture to be selected by url
        echo("selRequest();\n")
        echo("-->\n")
        echo("</script>\n")
        echo("</body>\n")

    def staticLegend(self):
        echo("<fieldset onclick='toggleLegend();'>\n")
        echo("<legend>Legend</legend>\n")
        echo("<table class='pval_table' title='Click here to show/hide legend'>")
        echo("<tr>\n")
        echo("<th>stats tests</th>\n")
        echo(
            "<td class='ok' style='height: 32px;'>OK&nbsp;(p-value&nbsp;&gt;"
            + self.pwarn
            + ")</td>"
        )
        echo("<td class='warn' style='height: 32px;'>WARN&nbsp;(" + self.pfail)
        echo("&lt;p-value&lt;" + self.pwarn + ")</td>\n")
        echo(
            "<td class='fail' style='height: 32px;'>FAIL&nbsp;(p-value&lt;"
            + self.pfail
            + ")</td>\n"
        )
        echo(
            "<td class='absent' style='height: 32px;'>&nbsp;&nbsp;ABSENT&nbsp;(no test)</td>\n"
        )
        echo("</tr>\n")
        echo("<tr>\n")
        echo("<td class='absent'>DCube status</td>")
        echo("<td class='plot'><span class='icon_green' alt='Green'></span> OK</td>")
        echo(
            "<td class='plot'><span class='icon_yellow' alt='Yellow'></span> WARN</td>"
        )
        echo("<td class='plot'><span class='icon_red' alt='Red'></span> FAIL</td>")
        echo("<td class='absent'> ABSENT (no status)</td>")
        echo("</tr></table>\n")
        echo("</fieldset>\n")

    #
    # footer - Valid CSS, Valid HTML icons + anchors to documentation, download etc.
    #
    def footer(self):
        echo(
            "<a href='https://twiki.cern.ch/twiki/bin/view/AtlasComputing/DCube'>[&nbsp;Documentation&nbsp;]</a>\n"
        )
        echo("<a href='https://validator.w3.org/check?uri=referer'>")
        echo("<img src='" + self.imgToUri("images/valid-html401-blue.png") + "' ")
        echo(
            "alt='Valid HTML 4.01 Strict' style='vertical-align: middle;' height='31' width='88'></a>\n"
        )
        echo("<a href='https://jigsaw.w3.org/css-validator/check/referer'>")
        echo("<img src='" + self.imgToUri("images/valid-css-blue.png") + "' ")
        echo("alt='Valid CSS!' style='vertical-align: middle;'></a>\n")

    #
    # @brief displays monitored and reference filenames, descriptions, time stamp etc.
    #
    def configurationPanel(self):
        mon = self.xml["dcube"][0]["monitored"][0]["attr"].get("file", "")
        ref = self.xml["dcube"][0]["reference"][0]["attr"].get("file", "")
        test_desc = self.xml["dcube"][0]["test_desc"][0].get("cdata", "")
        ref_desc = self.xml["dcube"][0]["ref_desc"][0].get("cdata", "")
        branch = self.xml["dcube"][0]["attr"].get("branch", "")
        install = self.xml["dcube"][0]["attr"].get("install", "")
        cmtconfig = self.xml["dcube"][0]["attr"].get("cmtconfig", "")
        project = self.xml["dcube"][0]["attr"].get("project", "")
        jobId = self.xml["dcube"][0]["attr"].get("jobid", "")
        date = self.xml["dcube"][0]["date"][0].get("cdata", "")
        if (
            mon
            and not os.path.isabs(mon)
            and os.path.isfile(os.path.join(self.outdir, mon))
        ):
            mon = "<a href='" + mon + "'>" + mon + "</a>"
        if (
            ref
            and not os.path.isabs(ref)
            and os.path.isfile(os.path.join(self.outdir, ref))
        ):
            ref = "<a href='" + ref + "'>" + ref + "</a>"
        if self.opts.monitoredName != "monitored":
            test_desc = self.opts.monitoredName
        elif not test_desc or test_desc == "PUT YOUR TEST DESCRIPTION HERE":
            test_desc = "&mdash;"
        if self.opts.referenceName != "reference":
            ref_desc = self.opts.referenceName
        elif not ref_desc or ref_desc == "PUT YOUR REFERENCE DESCRIPTION HERE":
            ref_desc = "&mdash;"
        echo("Test desc.:&nbsp;<span class='bold'>" + test_desc + "</span><br>\n")
        echo("Date:&nbsp;<span class='bold'>" + date + "</span><br>\n")
        echo("BRANCH:&nbsp;<span class='bold'>" + branch + "</span>&nbsp;")
        echo("INSTALL:&nbsp;<span class='bold'>" + install + "</span>&nbsp;\n")
        echo("CMTCONFIG:&nbsp;<span class='bold'>" + cmtconfig + "</span>&nbsp;\n")
        echo("PROJECT:&nbsp;<span class='bold'>" + project + "</span>&nbsp;\n")
        echo("JOB ID:&nbsp;<span class='bold'>" + jobId + "</span><br>\n")
        echo("Monitored root file:&nbsp;" + mon + "<br>\n")
        echo("Reference root file:&nbsp;" + ref + "<br>\n")
        echo(
            "Reference file descr.:&nbsp;<span class='bold'>"
            + ref_desc
            + "</span><br>\n"
        )
        if self.log_file != "":
            echo('Log file:&nbsp;<a href="' + self.log_file + '">[ as plain text ]</a>')
            echo('&nbsp;<a href="dcubelog.html">[ as HTML ]</a><br>')
        else:
            echo("Log file:<i>Log file not available.</i><br>\n")

    #
    # @brief produces histograms/directories DIV
    #
    def mainPanel(self):
        echo("<br>")
        # tabpanel bar
        echo("<div class='tabbar'>")
        # 1st button
        echo("<div id='t1' class='tabpane' onClick='selPane(\"1\");'>")
        echo("<div class='tp_left'></div>")
        echo("<div class='tp_title'>Normal view</div>")
        echo("<div class='tp_right'></div>")
        echo("</div>")
        # 2nd button
        echo("<div id='t2' class='tabpane' onClick='selPane(\"2\");'>")
        echo("<div class='tp_left'></div>")
        echo("<div class='tp_title'>Plots View</div>")
        echo("<div class='tp_right'></div>")
        echo("</div>")
        # 3rd button
        echo("<div id='t3' class='tabpane' onClick='selPane(\"3\");'>")
        echo("<div class='tp_left'></div>")
        echo("<div class='tp_title'>Summary</div>")
        echo("<div class='tp_right'></div>")
        echo("</div>")
        # 4th button
        echo("<div id='t4' class='tabpane' onClick='selPane(\"4\");'>")
        echo("<div class='tp_left'></div>")
        echo("<div class='tp_title'>Settings</div>")
        echo("<div class='tp_right'></div>")
        echo("</div>")
        # end of tabpanel
        echo("</div>")
        # content panel
        echo("<div class='contentPane' >\n")
        # normal, tree-like view
        echo("<div id='c1' class='tp'>")
        # header row
        echo("<div class='tdir'>")
        echo("<div class='tdir0'><div class='absent'>name</div></div>")
        echo("<div class='tdir1'><div class='absent'>K-S test</div></div>")
        echo("<div class='tdir1'><div class='absent'>&chi;&sup2; test</div></div>")
        echo("<div class='tdir1'><div class='absent'>\"bin-by-bin\" test</div></div>")
        echo("<div class='tdir1'><div class='absent'>DCube status</div></div>")
        echo("</div>\n")
        # directory tree
        rootDir = self.xml["dcube"][0]["tdirectory"][0]
        if type(rootDir) is dict:
            self.id = 0
            self.treeView(rootDir, 0)
        else:
            echo(
                "<span style='color: red;'> root (/) TDirectory not found in output xml file!</span><br>"
            )
        echo("</div>\n")
        # plots view
        echo("<div id='c2' class='tp' style='display: none; text-align: left;'>\n")
        self.plotView(rootDir, "/", "prow_1_1")
        echo("</div>\n")
        # summary view
        echo("<div id='c3' class='tp' style='display: none;'>")
        self.summaryViewDIV()
        echo("</div>\n")
        # settings
        echo("<div id='c4' class='tp' style='display: none;'>")
        self.settingsView()
        echo("</div>\n")
        echo("</div>")
        # end of content panel
        echo("<br>")

    def settingsView(self):
        echo("<fieldset>")
        echo("<legend>&nbsp;Display&nbsp;</legend>")
        echo("<div class='setup'> When loading DCube page show ")
        arr = {1: "Normal View", 2: "Plots View", 3: "Summary", 4: "Settings"}
        echo("<select id='gSelectPane' onChange='setupPane(this.value);'>")
        for key, value in arr.items():
            echo("<option value='" + str(key) + "'>" + value + "</option>")
        echo("</select> tabpane first. </div>")
        echo("</fieldset>")
        echo("<fieldset>")
        echo("<legend>&nbsp;Plots view&nbsp;</legend>")
        echo("<div class='setup'> Show ")
        echo("<select id='gSelectPlotsPerPage' onChange='setupGallery( this.value );'>")
        arr = [5, 10, 20, 50, 500]
        for value in arr:
            echo("<option value='" + str(value) + "'>" + str(value) + "</option>")
        echo("</select> plots per page <i>(triggers page reloading)</i></div>")
        echo("</fieldset>")

    #
    # @brief displays p-value test legend (transparend box in upper-right corner of the page)
    #
    def testLegend(self):
        echo(
            "<div title='Click here to hide legend' id='pval_legend' "
            + "class='pval_legend' onclick='toggleLegend();' style='display: none; background: #ffffff; z-index: 10000;'>"
        )
        echo("<table class='pval_table' title='Click here to hide legend'>")
        echo("<tr>\n")
        echo("<th>stats tests</th>\n")
        echo(
            "<td class='ok' style='height: 32px;'>OK&nbsp;(p-value&nbsp;&gt;"
            + self.pwarn
            + ")</td>"
        )
        echo("<td class='warn' style='height: 32px;'>WARN&nbsp;(" + self.pfail)
        echo("&lt;p-value&lt;" + self.pwarn + ")</td>\n")
        echo(
            "<td class='fail' style='height: 32px;'>FAIL&nbsp;(p-value&lt;"
            + self.pfail
            + ")</td>\n"
        )
        echo(
            "<td class='absent' style='height: 32px;'>&nbsp;&nbsp;ABSENT&nbsp;(no test)</td>\n"
        )
        echo("</tr>\n")
        echo("<tr>\n")
        echo("<td class='absent'>DCube status</td>")
        echo("<td class='plot'><span class='icon_green' alt='Green'></span> OK</td>")
        echo(
            "<td class='plot'><span class='icon_yellow' alt='Yellow'></span> WARN</td>"
        )
        echo("<td class='plot'><span class='icon_red' alt='Red'></span> FAIL</td>")
        echo("<td class='absent'> ABSENT (no status)</td>")
        echo("</tr></table>\n")
        echo("</div>\n")

    #
    # @brief Summary View content
    #
    def summaryViewDIV(self):
        summaryNode = self.xml["dcube"][0]["summary"][0]
        status = summaryNode["attr"].get("status")
        if status == "OK":
            status = "<span class='icon_green' alt='Green'></span>"
        if status == "WARN":
            status = "<span class='icon_yellow' alt='Yellow'></span>"
        if status == "FAIL":
            status = "<span class='icon_red' alt='Red'></span>"
        echo("<div class='si' style='text-align: center;'>")
        echo(
            " Overall status:&nbsp;"
            + status
            + "&nbsp;"
            + summaryNode["attr"]["status"]
            + "<br>"
        )
        echo(" Objects processed:&nbsp;" + summaryNode["attr"]["objs"])
        echo("</div><br>")
        echo("<div style='text-align: center;'>")
        echo("<table class='sumTable'>")
        nodeKS = summaryNode["table"][0]["tr"][1].get("td")
        rowKS = (
            "<td>"
            + str(nodeKS[1]["cdata"])
            + "</td>"
            + "<td>"
            + str(nodeKS[2]["cdata"])
            + "</td>"
            + "<td>"
            + str(nodeKS[3]["cdata"])
            + "</td></tr>"
        )
        nodeChi2 = summaryNode["table"][0]["tr"][2].get("td")
        rowChi2 = (
            "<td>"
            + str(nodeChi2[1]["cdata"])
            + "</td>"
            + "<td>"
            + str(nodeChi2[2]["cdata"])
            + "</td>"
            + "<td>"
            + str(nodeChi2[3]["cdata"])
            + "</td></tr>"
        )
        nodeBBB = summaryNode["table"][0]["tr"][3].get("td")
        rowBBB = (
            "<td>"
            + str(nodeBBB[1]["cdata"])
            + "</td>"
            + "<td>"
            + str(nodeBBB[2]["cdata"])
            + "</td>"
            + "<td>"
            + str(nodeBBB[3]["cdata"])
            + "</td></tr>"
        )
        nodeMeanY = summaryNode["table"][0]["tr"][4].get("td")
        rowMeanY = (
            "<td>"
            + str(nodeMeanY[1]["cdata"])
            + "</td>"
            + "<td>"
            + str(nodeMeanY[2]["cdata"])
            + "</td>"
            + "<td>"
            + str(nodeMeanY[3]["cdata"])
            + "</td></tr>"
        )
        nodeSUM = summaryNode["table"][0]["tr"][5].get("td")
        rowSUM = (
            "<td>"
            + str(nodeSUM[1]["cdata"])
            + "</td>"
            + "<td>"
            + str(nodeSUM[2]["cdata"])
            + "</td>"
            + "<td>"
            + str(nodeSUM[3]["cdata"])
            + "</td></tr>"
        )
        nodeFRAC = summaryNode["table"][0]["tr"][6].get("td")
        rowFRAC = (
            "<td>"
            + num(nodeFRAC[1]["cdata"], "0")
            + "%</td>"
            + "<td>"
            + num(nodeFRAC[2]["cdata"], "0")
            + "%</td>"
            + "<td>"
            + num(nodeFRAC[3]["cdata"], "0")
            + "%</td></tr>"
        )
        echo(
            "<tr><th>test</th><td class='ok'>OK</td>"
            + "<td class='warn'>WARN</td>"
            + "<td class='fail'>FAIL</td></tr>"
        )
        if summaryNode["attr"].get("nbksnorm") and not summaryNode["attr"].get("nbks"):
            echo("<tr><th>K-S + norm</th>" + rowKS)
        else:
            echo("<tr><th>K-S</th>" + rowKS)
        echo("<tr><th>&chi;&sup2;</th>" + rowChi2)
        echo('<tr><th>"bin-by-bin"</th>' + rowBBB)
        echo('<tr><th>"mean y"</th>' + rowMeanY)
        echo("<tr><th>sum</th>" + rowSUM)
        echo("<tr><th>fractions</th>" + rowFRAC)
        echo("</table>")
        echo("</div><br>")
        nbErrors = summaryNode["attr"].get("errors")
        echo("<div class='si'>")
        echo("&nbsp;Errors:&nbsp;" + str(nbErrors))
        errorsNode = summaryNode["errors"][0].get("error")
        if errorsNode and type(errorsNode) is list:
            echo("<ul>")
            for error in errorsNode:
                what = error["attr"].get("what")
                times = error["attr"].get("times")
                echo("<li>" + what + " - occurred " + times + " time(s)")
            echo("</ul>")
        echo("</div>")

    # tree-like view
    def treeView(self, dirNode=None, level=0):
        dirName = dirNode["attr"].get("name")
        displayName = self.displayName(dirName, level)
        tab = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" * level
        self.id += 1
        # set the counters for stat test
        dirNode["KS"] = {"OK": 0, "WARN": 0, "FAIL": 0}
        dirNode["chi2"] = {"OK": 0, "WARN": 0, "FAIL": 0}
        dirNode["bbb"] = {"OK": 0, "WARN": 0, "FAIL": 0}
        echo("<div id='d_" + str(self.id) + "' class='dir' ")
        if level <= 1:
            echo("style='display: block;' ")
        else:
            echo("style='display: revert;' ")
        echo(
            " title='"
            + dirName
            + "' onmouseover=\"statusBar('TDirectory "
            + dirName
            + "');\""
            + "onclick=\"showhide('dc_"
            + str(self.id)
            + "', 'di_"
            + str(self.id)
            + "');\">"
        )
        echo("<div class='dir0'>" + tab)
        if dirName != "/":
            echo(
                "<span id='di_"
                + str(self.id)
                + "' class='icon_tdirectory_c'"
                + "alt='folder icon'></span>&nbsp;"
            )
        else:
            echo(
                "<span id='di_"
                + str(self.id)
                + "' class='icon_tdirectory_o'"
                + "alt='folder icon'></span>&nbsp;"
            )
        echo(displayName)
        echo("</div>")
        # summary tables
        echo("<div class='dir1' id='KS_" + str(self.id) + "'>")
        self.sumDirTABLE(dirNode, "ks")
        echo("</div>")
        echo("<div class='dir1' id='chi2_" + str(self.id) + "'>")
        self.sumDirTABLE(dirNode, "chi2")
        echo("</div>")
        echo("<div class='dir1' id='bbb_" + str(self.id) + "'>")
        self.sumDirTABLE(dirNode, "bbb")
        echo("</div>")
        # DCube processing status
        status = dirNode["attr"].get("status")
        echo("<div class='dir1'>")
        if "OK" in status:
            echo(
                "<span class='icon_green' alt='Green' id='dqb_"
                + str(self.id)
                + "'></span>"
            )
        else:
            if "WARN" in status:
                echo(
                    "<span class='icon_yellow' alt='Yellow' id='dqb_"
                    + str(self.id)
                    + "'></span>"
                )
            else:
                echo(
                    "<span class='icon_red' alt='Red' id='dqb_"
                    + str(self.id)
                    + "'></span>"
                )
        echo("</div>")
        echo("</div>\n")
        if level < 1:
            echo(
                "<div id='dc_"
                + str(self.id)
                + "' class='dirc' style='display: revert;'>"
            )
        else:
            echo(
                "<div id='dc_" + str(self.id) + "' class='dirc' style='display: none;'>"
            )
        subDirs = dirNode.get("tdirectory")
        if type(subDirs) is list:
            for subdir in subDirs:
                self.treeView(subdir, level + 1)
        elif type(subDirs) is dict:
            for _nb, subdir in subDirs.items():
                self.treeView(subdir, level + 1)
        hists = dirNode.get("plot")
        if type(hists) is list:
            for hist in hists:
                self.histDiv(hist, level + 1, dirNode)
        echo("</div>\n")

    def sumDirTABLE(self, dirNode=None, test=None):
        echo("<table class='st'>")
        echo("<tr>")
        testOK = dirNode["attr"].get(test + "ok", "0")
        if testOK != "0":
            echo("<td class='ok'>" + testOK + "</td>")
        else:
            echo("<td class='eok'>&mdash;</td>")
        testWARN = dirNode["attr"].get(test + "warn", "0")
        if testWARN != "0":
            echo("<td class='warn'>" + testWARN + "</td>")
        else:
            echo("<td class='ewarn'>&mdash;</td>")
        testFAIL = dirNode["attr"].get(test + "fail", "0")
        if testFAIL != "0":
            echo("<td class='fail'>" + testFAIL + "</td>")
        else:
            echo("<td class='efail'>&mdash;</td>")
        echo("</tr></table>")

    def histDiv(self, hist=None, level=None, parDir=None):
        self.id += 1
        tab = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" * level
        histName = hist["attr"].get("name")
        displayName = self.displayName(histName, level)
        type = hist["attr"].get("type")
        # DCube status
        tests = hist["attr"].get("stest")
        plots = hist["attr"].get("plots")
        stat = hist["attr"].get("status")
        regSrc = hist["img"][0]["attr"].get("src") if "img" in hist else ""
        self.srcTable[regSrc] = self.plotToUri(regSrc)
        picInfo = ""
        if stat and "FAIL" in stat:
            status = stat.split(";")
            picInfo += "STATUS: " + status[0] + " &mdash; " + status[1]
        else:
            picInfo += "TESTS: " + tests
        self.picTableJS += (
            "picTable[" + str(self.picNum) + "]=getPlot('" + regSrc + "');\n"
        )
        self.picInfoTableJS += (
            "picInfoTable["
            + str(self.picNum)
            + "]='<span>&#35;"
            + str(self.picNum + 1)
            + " "
            + picInfo
            + r"<\/span>';"
            + "\n"
        )
        self.picNum += 1
        showhide = (
            " showhide('hc_"
            + str(self.id)
            + "', null, 'plot_"
            + str(self.id)
            + "', "
            + "getPlot('"
            + regSrc
            + "')"
            + ");"
        )
        # allow for picture to be selected by url
        rid = "r_" + str(self.id)
        if "TH1" in type:
            icon = "h1"
        if "TH2" in type:
            icon = "h2"
        if "TGraph" in type or "TEfficiency" in type:
            icon = "graph"
        if "TProfile" in type:
            icon = "profile"
        if level == 1:
            echo(
                "<div id='"
                + rid
                + "' class='his' style='display: block;' title='"
                + histName
                + "' "
                + "onmouseover=\"statusBar('"
                + type
                + " "
                + histName
                + '\');" onclick="'
                + showhide
                + '">'
            )
        else:
            echo(
                "<div id='"
                + rid
                + "' class='his' style='display: revert;' title='"
                + histName
                + "' "
                + "onmouseover=\"statusBar('"
                + type
                + " "
                + histName
                + '\');" onclick="'
                + showhide
                + '">'
            )
        echo(
            "<div class='his0'>"
            + tab
            + "<span class='icon_"
            + icon
            + "'"
            + "></span>&nbsp;"
            + displayName
        )
        echo("</div>")
        pvalNode = hist["stat"][0].get("pvalue", []) if "stat" in hist else []
        pvals = {p["attr"].get("test"): p.get("cdata") for p in pvalNode}
        stats = {p["attr"].get("test"): p["attr"].get("status") for p in pvalNode}
        for k in ["KS", "chi2", "bbb"]:
            pvals.setdefault(k)
            stats.setdefault(k)
        if "TProfile" in type:
            echo("<div class='his1' style='width: 48.5%;'>")
            pval = (
                "<span style='font-size: 8pt;'>|&mu;<sup>eff</sup>(mon) - &mu;<sup>eff</sup>(ref)| = "
                + pvals.get("meanY", "")
                + "</span>"
            )
            if stats.get("meanY", "") == "OK":
                echo("<div class='ok' style='margin: auto auto;'>" + pval + "</div>")
            else:
                if stats.get("meanY", "") == "WARN":
                    echo(
                        "<div class='warn' style='margin: auto auto;'>"
                        + pval
                        + "</div>"
                    )
                else:
                    if stats.get("meanY", "") == "FAIL":
                        echo(
                            "<div class='fail' style='margin: auto auto;'>"
                            + pval
                            + "</div>"
                        )
                    else:
                        echo(
                            "<div class='absent' style='margin: auto auto;'>&mdash;</div>"
                        )
            echo("</div>")
        else:
            self.pvalueDIV(pvals["KS"], stats["KS"])
            self.pvalueDIV(pvals["chi2"], stats["chi2"])
            self.pvalueDIV(pvals["bbb"], stats["bbb"])
        # DCube status
        tests = hist["attr"].get("stest", "")
        plots = hist["attr"].get("plots", "")
        stat = hist["attr"].get("status", "")
        status = stat + tests + plots
        echo("<div title='DCube status' class='his1'>")
        if "FAIL" in status:
            echo("<span class='icon_red' alt='Red'></span>")
        else:
            if "WARN" in status:
                echo("<span class='icon_yellow' alt='Yellow'></span>")
            else:
                echo("<span class='icon_green' alt='Green'></span>")
        echo("</div>")
        echo("</div>")
        self.histContentsDIV(hist, self.id)

    def svgEncode(self, svg):
        # Stackoverflow: https://stackoverflow.com/a/66718254/1928287
        # Ref: https://bl.ocks.org/jennyknuth/222825e315d45a738ed9d6e04c7a88d0
        # Encode an SVG string so it can be embedded into a data URL.
        enc_chars = '"%#{}<>'  # Encode these to %hex
        ### enc_chars_maybe = "&|[]^`;?:@="  # Add to enc_chars on exception
        svg_enc = ""
        # Translate character by character
        for c in str(svg):
            if c in enc_chars:
                if c == '"':
                    svg_enc += "'"
                else:
                    svg_enc += "%" + format(ord(c), "x")
            else:
                svg_enc += c
        return " ".join(svg_enc.split())  # Compact whitespace

    def imgToUri(self, file):
        with share_path(os.path.join("DCubeServer", file)) as path:
            with open(path, "rb") as f:
                if file.endswith(".gif"):
                    data = f.read()
                    data = base64.b64encode(data).decode("utf8")
                    return "data:image/gif;base64," + data
                if file.endswith(".png"):
                    data = f.read()
                    data = base64.b64encode(data).decode("utf8")
                    return "data:image/png;base64," + data
                elif file.endswith(".svg"):
                    data = f.read()
                    data = base64.b64encode(data).decode("utf8")
                    return "data:image/svg+xml;base64," + data

    def plotToUri(self, file):
        if not self.opts.inline_plots or file == "":
            return file
        file = os.path.join(self.opts.tempdir, file)
        with open(file, "rb") as f:
            if file.endswith(".svg"):
                data = f.read().decode("utf8")
                data = self.svgEncode(data)
                return "data:image/svg+xml;utf8," + data
            elif file.endswith(".png"):
                data = f.read()
                data = base64.b64encode(data).decode("utf8")
                return "data:image/png;base64," + data

    #
    # @brief display DIV with histogram/plot or profile contents
    #
    def histContentsDIV(self, histNode=None, id=None):
        type = histNode["attr"].get("type")
        name = histNode["attr"].get("name")
        status = histNode["attr"].get("status")
        echo("\n<div id='hc_" + str(id) + "' class='hc' style='display: none;'>")
        echo(
            "<span style='font-size: 14pt;'>Name: <tt>"
            + name
            + "</tt>&nbsp;&nbsp;Type: <tt>"
            + type
            + "</tt></span><br><br>"
        )
        if status:
            status = status.split(";")
            echo(
                "<span style='color: red'>"
                + status[0]
                + " &mdash; "
                + status[1]
                + "</span><br>"
            )
        self.statbox(histNode, type)
        imgNodes = histNode.get("img")
        if imgNodes is not None:
            echo("<div class='tabbar'>")
            pid = "plot_" + str(self.id)
            reg = ""
            for i, val in enumerate(imgNodes):
                src = val["attr"].get("src")
                self.srcTable[src] = self.plotToUri(src)
                src = 'getPlot("' + src + '")'
                type = val["attr"].get("type")
                if reg == "":
                    reg = src
                id = type + "_" + str(self.id)
                echo(
                    "<div id='"
                    + id
                    + "' class='tabpane"
                    + (" sel_tp" if reg in type or i == 0 else "")
                    + "' onClick='selPlot(\""
                    + id
                    + '", '
                    + src
                    + ");'>"
                )
                if "reg" in type:
                    echo("<div class='tp_left'></div>")
                    echo("<div class='tp_title'>Normal plot</div>")
                    echo("<div class='tp_right'></div>")
                    echo("</div>")
                else:
                    echo("<div class='tp_left'></div>")
                    if type == "log":
                        type = "Log"
                    if type == "dif":
                        type = "Diff"
                    if type == "prx":
                        type = "Proj X"
                    if type == "pry":
                        type = "Proj Y"
                    type += " plot"
                    echo("<div class='tp_title'>" + type + "</div>")
                    echo("<div class='tp_right'></div>")
                    echo("</div>")
            echo("</div>")
            echo("<div class='contentPane'>\n")
            echo("<div class='tp'>")
            # provide a link to this picture
            echo(
                "<a href='?rid=r_"
                + str(self.id)
                + "'><img class='plot' alt='plot' id='"
                + pid
                + "' src='"
                + self.loading_src
                + "'></a>"
            )

            if self.opts.inline_plots:
                echo(
                    "<br><a id='"
                    + pid
                    + "_download' href='' download='"
                    + name
                    + "'>Download</a>"
                )
            echo("</div>")
            echo("</div>")
        echo("</div>")

    #
    # @param pval pvalue
    # @param status test status
    #
    def pvalueDIV(self, pval=None, status=None):
        echo("<div class='his1'>")
        if status == "OK":
            echo("<div class='ok'>" + pval + "</div>")
        else:
            if status == "WARN":
                echo("<div class='warn'>" + pval + "</div>")
            else:
                if status == "FAIL":
                    echo("<div class='fail'>" + pval + "</div>")
                else:
                    echo("<div class='absent'>&mdash;</div>")
        echo("</div>")

    # change the display name if longer than 35 characters
    def displayName(self, name=None, level=None):
        w = (level - 1) * 5
        if len(name) + w > 35:
            w = 35 - w + (level - 1) * 3 - 1
            if w + 8 < len(name):
                beg = name[0:w]
                end = name[-3:]
                name = beg + "(...)" + end
        return name

    #
    # function to display "plot view"
    #
    def plotView(self, object=None, dirname=None, pid=None):
        echo("<div class='gal'>")
        # top bar
        echo("<div class='gbar' id='gtop'>")
        echo("<span id='gtt'></span>")
        echo(
            "<div class='gnav' id='gsbt' onClick='gGetBeg();' title='Begin'><span>&lt;&lt;&lt;</span></div>"
        )
        echo(
            "<div class='gnav' id='gspt' onClick='gGetPrev();' title='Previous'><span>&lt;</span></div>"
        )
        echo("<div class='gnav' id='gjt' title='Jump to...'>Jump: ")
        echo("<select id='gst' onchange='gOnSelect(this.value);'>")
        if self.picNum > 0:
            echo("<option value='0'>1 - " + str(self.picNum) + "</option>")
        echo("</select>")
        echo("</div>")
        echo(
            "<div class='gnav' id='gsnt' onClick='gGetNext();' title='Next'><span>&gt;</span></div>"
        )
        echo(
            "<div class='gnav' id='gset' onClick='gGetEnd();' title='End'><span>&gt;&gt;&gt;</span></div>"
        )
        echo("</div>")
        # galery content
        echo("<div class='gc'>")
        i = 0
        while i < self.picNum:
            echo(
                "\n<div class='gp' id='gp_"
                + str(i)
                + "'><div class='gs' id='gs_"
                + str(i)
                + "'></div>"
            )
            echo("<img class='gi' id='gi_" + str(i) + "' src='' alt='plot'></div>")
            i += 1
        echo("</div>\n")
        # bottom bar
        echo("<div class='gbar' id='gbottom'>")
        echo("<span id='gbt'></span>")
        echo(
            "<div class='gnav' id='gsbb' onClick='gGetBeg();' title='Begin'><span>&lt;&lt;&lt;</span></div>"
        )
        echo(
            "<div class='gnav' id='gspb' onClick='gGetPrev();' title='Previuos'><span>&lt;</span></div>"
        )
        echo("<div class='gnav' id='gjb' title='Jump to...'>Jump: ")
        echo("<select id='gsb' onchange='gOnSelect(this.value);'>")
        if self.picNum > 0:
            echo("<option value='0'>1 - " + str(self.picNum) + "</option>")
        echo("</select>")
        echo("</div>\n")
        echo(
            "<div class='gnav' id='gsnb' onClick='gGetNext();' title='Next'><span>&gt;</span></div>"
        )
        echo(
            "<div class='gnav' id='gseb' onClick='gGetEnd();' title='End'><span>&gt;&gt;&gt;</span></div>"
        )
        echo("</div>")
        # end of gallery
        echo("</div>\n")

    #
    # @brief produces basic statistic div
    # @param object XML node
    # @param type root type
    #
    def statbox(self, object=None, type=None):
        obj = object.get("stat", [None])[0]
        if obj is None:
            return
        if "TGraph" in type or (
            "TEfficiency" in type and len(obj.get("dim", [])) != 2
        ):  # 2D TEfficiency handled later
            if "points" in obj:
                p = obj["points"][0].get("cdata")
                rp = obj["points"][0]["attr"].get("ref")
                m = obj["mean"][0].get("cdata")
                rm = obj["mean"][0]["attr"].get("ref")
                r = obj["rms"][0].get("cdata")
                rr = obj["rms"][0]["attr"].get("ref")
            else:
                p, rp, m, rm, r, rr = [None] * 6
            echo("<table class='center' style='width: 60%; font-size: 10pt;'>")
            echo("<tr><th></th><th>monitored</th><th>reference</th></tr>")
            echo("<tr><td class='bl'>Points</td><td class='bc'>" + num(p) + "</td>")
            echo("<td class='bc'>" + num(rp) + "</td></tr>")
            echo("<tr><td class='yl'>Mean</td><td class='yc'>" + num(m) + "</td>")
            echo("<td class='yc'>" + num(rm) + "</td></tr>")
            echo("<tr><td class='bl'>RMS</td><td class='bc'>" + num(r) + "</td>")
            echo("<td class='bc'>" + num(rr) + "</td></tr>")
            echo("</table>\n")

        else:

            def two(a, d=""):
                return (d if x is None else x for x in (tuple(a) + (None, None))[:2])

            def sumnum(a, d="&mdash;", op=lambda x: sum(x)):
                if all(x is None or x == "" for x in a):
                    return d
                return num(op(0.0 if x is None or x == "" else float(x) for x in a))

            def con(a, d="&mdash;"):
                if not a or a[0] is None or a[0] == "":
                    return d
                return "".join(a)

            e, re = (
                two((obj["entries"][0]["cdata"], obj["entries"][0]["attr"].get("ref")))
                if "entries" in obj
                else ("", "")
            )
            od = obj.get("dim")
            if od:
                xbin, ybin = two(o["attr"].get("bins") for o in od)
                xu, yu = two(o["underflow"][0]["cdata"] for o in od)
                xo, yo = two(o["overflow"][0]["cdata"] for o in od)
                rxu, ryu = two(o["underflow"][0]["attr"].get("ref") for o in od)
                rxo, ryo = two(o["overflow"][0]["attr"].get("ref") for o in od)
                xm, ym = two(o["mean"][0].get("cdata") for o in od)
                xm_unc, ym_unc = two(o["mean_unc"][0].get("cdata") for o in od)
                rxm, rym = two(o["mean"][0]["attr"].get("ref") for o in od)
                rxm_unc, rym_unc = two(o["mean_unc"][0]["attr"].get("ref") for o in od)
                xrms, yrms = two(o["rms"][0].get("cdata") for o in od)
                xrms_unc, yrms_unc = two(o["rms_unc"][0].get("cdata") for o in od)
                rxrms, ryrms = two(o["rms"][0]["attr"].get("ref") for o in od)
                rxrms_unc, ryrms_unc = two(
                    o["rms_unc"][0]["attr"].get("ref") for o in od
                )
            else:
                xbin, ybin, xu, yu, xo, yo, rxu, ryu, rxo, ryo = [None] * 10
                xm, ym, xm_unc, ym_unc, rxm, rym = [None] * 6
                rxm_unc, rym_unc, xrms, yrms, xrms_unc, yrms_unc = [None] * 6
                rxrms, ryrms, rxrms_unc, ryrms_unc = [None] * 4

            echo("<table class='center' style='width: 60%; font-size: 10pt;'>")
            echo("<tr><th></th><th>monitored</th><th>reference</th></tr>")
            echo("<tr><td class='bl'>Entries</td><td class='bc'>" + num(e) + "</td>")
            echo("<td class='bc'>" + num(re) + "</td></tr>")
            echo(
                "<tr><td class='yl'>Underflows</td><td class='yc'>"
                + sumnum((xu, yu))
                + "</td>"
            )
            echo("<td class='yc'>" + sumnum((rxu, ryu)) + "</td></tr>")
            echo(
                "<tr><td class='bl'>Overflows</td><td class='bc'>"
                + sumnum((xo, yo))
                + "</td>"
            )
            echo("<td class='bc'>" + sumnum((rxo, ryo)) + "</td></tr>")
            # x
            echo(
                "<tr><td class='yl'>Bins(x)</td><td class='yc' colspan='2'>"
                + num(xbin)
                + "</td></tr>"
            )
            echo(
                "<tr><td class='bl'>Mean(x)</td><td class='bc'>"
                + con((xm, "&plusmn;", xm_unc))
                + "</td>"
            )
            echo("<td class='bc'>" + con((rxm, "&plusmn;", rxm_unc)) + "</td></tr>")
            echo(
                "<tr><td class='yl'>RMS(x)</td><td class='yc'>"
                + con((xrms, "&plusmn;", xrms_unc))
                + "</td>"
            )
            echo("<td class='yc'>" + con((rxrms, "&plusmn;", rxrms_unc)) + "</td></tr>")
            if "2" in type or "TProfile" in type or "TEfficiency" in type:
                # y
                echo(
                    "<tr><td class='bl'>Bins(y)</td><td class='bc' colspan='2'>"
                    + num(ybin)
                    + "</td></tr>"
                )
                echo(
                    "<tr><td class='yl'>Mean(y)</td><td class=\"yc\">"
                    + con((ym, "&plusmn;", ym_unc))
                    + "</td>"
                )
                echo("<td class='yc'>" + con((rym, "&plusmn;", rym_unc)) + "</td></tr>")
                echo(
                    "<tr><td class='bl'>RMS(y)</td><td class='bc'>"
                    + con((yrms, "&plusmn;", yrms_unc))
                    + "</td>"
                )
                echo("<td class='bc'>")
                echo(con((ryrms, "&plusmn;", ryrms_unc)))
                echo("</td></tr>")
            echo("</table>\n")


# ========================================================================================


# @class DCubeXmlParser
# @author Krzysztof Daniel Ciba (Krzysztof.Ciba@NOSPAMgmail.com)
#
# @brief An awful XML parser.
# We keep this class here because it is only used by DCubeHtmlWriter.
class DCubeXmlParser:
    parser = None
    data = None
    currTag = None
    tagStack = None

    # Initialise parser
    def __initParser(self):
        self.parser = xml.parsers.expat.ParserCreate()
        self.parser.StartElementHandler = self.__tag_open
        self.parser.EndElementHandler = self.__tag_close
        self.parser.CharacterDataHandler = self.__cdata
        self.data = {}
        self.currTag = self.data
        self.tagStack = []

    # Get data and reset parser
    def __getData(self):
        self.parser = None
        self.currTag = None
        self.tagStack = None
        data, self.data = (self.data, None)
        return data

    # main worker here
    def ParseFile(self, fp):
        self.__initParser()
        self.parser.ParseFile(fp)
        self.parser = None
        return self.__getData()

    def Parse(self, s):
        self.__initParser()
        self.parser.Parse(s)
        return self.__getData()

    # function to run if opening tag is found
    def __tag_open(self, name=None, attribs=None):
        name = name.lower()
        tagname = name
        # keep all plots together in one list, so we maintain the order in the xml
        if name == "hist1d" or name == "hist2d" or name == "graph":
            name = "plot"
        if not (name in self.currTag):
            self.currTag[name] = []
        newTag = {"attr": {}}
        if attribs is not None:
            # give default icons for TH1 or TH2 or TGraph
            newTag["attr"]["type"] = "TH1"
            if "hist2d" in tagname:
                newTag["attr"]["type"] = "TH2"
            if "graph" in tagname:
                newTag["attr"]["type"] = "TGraph"
            # end fix
            for attrname, attrvalue in attribs.items():
                newTag["attr"][attrname.lower()] = attrvalue
        self.currTag[name].append(newTag)
        t = self.currTag[name]
        self.currTag = t[len(t) - 1]
        self.tagStack.append(name)

    # function to store CDATA into array
    def __cdata(self, cdata=None):
        cdata = cdata.strip()
        if cdata:
            if "cdata" in self.currTag:
                self.currTag["cdata"] += cdata
            else:
                self.currTag["cdata"] = cdata

    # function to run if closing tag is found
    def __tag_close(self, name=None):
        self.currTag = self.data
        self.tagStack.pop()
        i = 0
        while i < len(self.tagStack):
            t = self.currTag[self.tagStack[i]]
            self.currTag = t[len(t) - 1]
            i += 1


# ========================================================================================


# @class DCubeLogWriter
# @author Krzysztof Daniel Ciba (Krzytof.Ciba@NOSPAMgmail.com)
# @brief <i>Rainbow Warrior</i>, log file coloriser
class DCubeLogWriter(DCubeObject):
    ln = 1
    lcount = {
        "VERBOSE": 0,
        "DEBUG": 0,
        "INFO": 0,
        "WARNING": 0,
        "ERROR": 0,
        "FATAL": 0,
        "CRITICAL": 0,
        "DEFAULT": 0,
    }
    log = "dcube.log"
    html = None  # -> stdout
    outdir = "."

    def __init__(self, log=None, html=None):
        if log:
            self.log = log
        if html:
            self.html = html
            self.outdir = os.path.dirname(self.html)

    def dcubeLOG(self):
        if self.html:
            html_fp = open(self.html, "w")
        else:
            html_fp = None  # -> sys.stdout

        def echo_html(*args):
            print(*args, end="", file=html_fp)

        global echo
        echo = echo_html  # make global function for all of DCubeHtmlWriter.py

        echo(
            '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" '
            + '"http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">\n\n'
        )
        echo("<html>\n")
        self.header()
        self.body()
        echo("</html>\n")

        echo = None
        html_fp = None  # close file

    def header(self):
        echo("<head>\n")
        echo("<title>RainbowWarrior LogBook</title>\n")
        echo('<meta http-equiv="content-type" content="text/html; charset=utf-8">\n')
        echo('<meta name="description" content="displaying log files as html">\n')
        echo('<meta name="keywords" content="Gaudi Athena log file colour">\n')
        echo('<meta http-equiv="content-language" content="en">\n')
        echo('<meta name="author" content="Krzysztof Daniel Ciba">\n')
        echo("<style>\n")
        echo("<!--\n")
        echo(
            "body { background-color: #e6e6e6; font-family: Courier, serif; font-size: 12pt; "
        )
        echo(" font-weight: normal; height: 100%; }\n")
        echo("span, div   { width: 100%;  }\n")
        echo("a { color: #0033ff; }\n")
        echo("a:visited {color: #ff0000;  }\n")
        echo("div.log     { padding-bottom: 95px; }\n")
        echo(
            "div.bottom  { font-weight: bold; height: 90px; color: #ffffff; background-color: #003399; "
        )
        echo("position: fixed; bottom: 0px; left: 0px; text-align: center; }\n")
        echo(".v { color: #999999; white-space: pre; }\n")
        echo(".d { color: #333333; white-space: pre; }\n")
        echo(".i { color: #000033; font-weight: bold; white-space: pre; }\n")
        echo(".w { color: #ff6600; font-weight: bold; white-space: pre; }\n")
        echo(".e { color: #cc0000; font-weight: bold; white-space: pre; }\n")
        echo(".f { color: #990000; font-weight: bold; white-space: pre; }\n")
        echo(".c { text-align: right; }\n")
        echo("-->\n")
        echo("</style>\n")
        echo("</head>\n")

    def body(self):
        f = open(self.log, "r")
        if not f:
            self.error("Cannot open log file " + self.log)
            return
        else:
            echo('<div class="log">\n')
            echo("<table>\n")
            for line in f:
                line = escape(line.rstrip(), quote=False)
                if not line:
                    continue
                echo(
                    '<tr><td class="c"><a href="#'
                    + str(self.ln)
                    + '" name="'
                    + str(self.ln)
                    + '">'
                    + str(self.ln)
                    + "</a>&nbsp;</td><td>"
                )
                self.ln += 1
                if "VERBOSE" in line:
                    echo('<span class="v">' + line + "</span></td></tr>\n")
                    self.lcount["VERBOSE"] += 1
                elif "DEBUG" in line:
                    echo('<span class="d">' + line + "</span></td></tr>\n")
                    self.lcount["DEBUG"] += 1
                elif "INFO" in line:
                    echo('<span class="i">' + line + "</span></td></tr>\n")
                    self.lcount["INFO"] += 1
                elif "WARNING" in line:
                    echo('<span class="w">' + line + "</span></td></tr>\n")
                    self.lcount["WARNING"] += 1
                elif "ERROR" in line:
                    echo('<span class="e">' + line + "</span></td></tr>\n")
                    self.lcount["ERROR"] += 1
                elif "FATAL" in line or "CRITICAL" in line or "Traceback" in line:
                    echo('<span class="f">' + line + "</span></td></tr>\n")
                    self.lcount["FATAL"] += 1
                else:
                    echo("<span>" + line + "</span></td></tr>\n")
                    self.lcount["DEFAULT"] += 1
            echo("</table>\n")
            echo("</div>\n")
            echo('<div class="bottom">')
            echo("<br>Number of lines with:<br>")
            echo(
                '<span class="v">VERBOSE = </span>'
                + str(self.lcount["VERBOSE"])
                + " | \n"
            )
            echo('<span class="d">DEBUG = </span>' + str(self.lcount["DEBUG"]) + "| \n")
            echo('<span class="i">INFO = </span>' + str(self.lcount["INFO"]) + " | \n")
            echo(
                '<span class="w">WARNING = </span>'
                + str(self.lcount["WARNING"])
                + " | \n"
            )
            echo(
                '<span class="e">ERROR = </span>' + str(self.lcount["ERROR"]) + " | \n"
            )
            echo(
                '<span class="f">FATAL/CRITICAL/Traceback = </span>'
                + str(self.lcount["FATAL"])
                + " | \n"
            )
            echo(
                '<span class="v">OTHERS = </span>'
                + str(self.lcount["DEFAULT"])
                + "<br> \n"
            )
            echo(
                '<a href="'
                + self.relpath(self.log)
                + '" target="_blank" style="color: #ffcc00" >click here to download log'
            )
            echo(" file as plain text</a><br>\n")
            echo("</div>\n")

        f.close()


## simple conversion from dcube.xml to dcube.html
if __name__ == "__main__":
    xml_file = sys.argv[1] if len(sys.argv) >= 2 else None
    name, ext = os.path.splitext(xml_file) if xml_file else ("", "")
    html_file = (
        sys.argv[2]
        if len(sys.argv) >= 3
        else os.path.join(
            os.path.dirname(xml_file),
            "dcubelog.html" if ext == ".log" else "dcube.html",
        )
        if xml_file
        else None
    )
    if ext == ".log":
        DCubeLogWriter(xml_file, html_file).dcubeLOG()
    else:
        DCubeHtmlWriter(xml_file=xml_file, html_file=html_file).dcubeHTML()
