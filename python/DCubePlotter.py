#!/bin/env python

# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
##
# @file DCubeClient/python/DCubePlotter.py
# @author Krzysztof Daniel Ciba (Krzyszotf.Ciba@NOSPAgmail.com)
# @brief implementation of DCubePlotter, DCubeStyle and test_DCubePlotter classes
#
# Bez wzgledu na to jak idiotoodporny jest twoj program, zawsze znajdzie
# sie wiekszy idiota.
#

import os, math
import ctypes
import uuid
import ROOT
from .DCubeUtils import DCubeObject
import unittest
import warnings

##
# @class DCubePlotter
# @author Krzysztof Daniel Ciba (Krzysztof.Ciba@NOSPAMgmail.com)
# @brief root plotter for DCubeClient package


class DCubePlotter(DCubeObject):
    ## DCubePlotter TCanvas
    canvas = None

    ## TPaveText with configuration
    configPave = None

    ## additional plot options
    plotOpts = []

    ## unzoom factor for yspread
    unzoom = 1.4

    ## c'tor
    # @param self "Me, myself and Irene"
    # @param xmldoc DOM XML Document instance
    # @param parsed tuple with args and opts from option parser
    def __init__(self, xmldoc, parsed):
        super(DCubePlotter, self).__init__(self)
        self.debug("constructing DCubePlotter...")

        self.xmldoc = xmldoc
        self.opts, self.args = parsed

        if self.opts.inline_plots:
            # use temporary directory to write inline plots
            self.outputDir = os.path.join(self.opts.tempdir, "plots")
        else:
            self.outputDir = os.path.join(os.path.dirname(self.opts.output), "plots")
        self.info("will store all plots in directory %s" % self.outputDir)

        try:
            if not os.path.exists(self.outputDir):
                os.mkdir(self.outputDir)

        except OSError:
            self.error(
                "cannot create output plot directory, plot creation will be disabled"
            )
            self.opts._update_careful({"makeplots": False})

        self.__applyTStyle()

    ## prepare/return ROOT.TCanvas instance
    # @param self "Me, myself and Irene"
    def __getCanvas(self):
        # if ( self.canvas ): del self.canvas
        # suggested by Time Adye: 1/05/2011
        if self.canvas:
            self.canvas.IsA().Destructor(self.canvas)

        self.canvas = ROOT.TCanvas("dcube", "dcube", 600, 600)
        if "logx" in self.plotOpts:
            self.canvas.SetLogx(1)
        if "logy" in self.plotOpts:
            self.canvas.SetLogy(1)
        if "logz" in self.plotOpts:
            self.canvas.SetLogz(1)
        return self.canvas

    ## set DCube plot dtaw style
    # @param self "Me, myself and Irene"
    def __applyTStyle(self, name="DCube"):
        self.debug("setting up DCube plot style...")

        ROOT.gStyle.SetOptTitle(0)
        ROOT.gStyle.SetOptStat(0)
        ROOT.gStyle.SetOptFit(0)

        ROOT.gStyle.SetFillStyle(1001)

        ROOT.gStyle.SetMarkerSize(0.8)
        ROOT.gStyle.SetMarkerStyle(ROOT.kFullCircle)

        ROOT.gStyle.SetLabelFont(42, "xyz")
        ROOT.gStyle.SetLabelSize(0.03, "xyz")

        ROOT.gStyle.SetTitleFont(42, "xyz")
        ROOT.gStyle.SetTitleFontSize(0.04)

        ROOT.gStyle.SetTitleFont(42, "a")

        ROOT.gStyle.SetFuncStyle(1)
        ROOT.gStyle.SetFuncWidth(2)

        ROOT.gStyle.SetHistLineColor(ROOT.kBlack)
        ROOT.gStyle.SetHistFillColor(ROOT.kRed)
        ROOT.gStyle.SetHistLineWidth(1)

        ROOT.gStyle.SetPadBorderSize(1)
        ROOT.gStyle.SetPadBorderMode(0)

        ROOT.gStyle.SetPadLeftMargin(0.1)
        ROOT.gStyle.SetPadBottomMargin(0.1)
        ROOT.gStyle.SetPadRightMargin(0.1)
        ROOT.gStyle.SetPadTopMargin(0.1)

        ROOT.gStyle.SetCanvasBorderSize(1)
        ROOT.gStyle.SetCanvasBorderMode(0)

        ROOT.gStyle.SetGridStyle(3)
        ROOT.gStyle.SetGridWidth(1)

        ROOT.gStyle.SetOptDate(21)
        ROOT.gStyle.GetAttDate().SetTextFont(42)
        ROOT.gStyle.GetAttDate().SetTextSize(0.025)

        ROOT.gStyle.SetLegendBorderSize(1)

        ROOT.gStyle.SetTextColor(ROOT.kBlack)
        ROOT.gStyle.GetAttDate().SetTextColor(ROOT.kBlack)
        ROOT.gStyle.SetLabelColor(ROOT.kBlack, "xyz")
        ROOT.gStyle.SetTitleColor(ROOT.kBlack, "xyz")

        ROOT.gStyle.SetFillColor(ROOT.kWhite)
        ROOT.gStyle.SetMarkerColor(ROOT.kBlue)
        ROOT.gStyle.SetCanvasColor(ROOT.kWhite)

        ROOT.gStyle.SetFrameFillColor(ROOT.kWhite)
        ROOT.gStyle.SetPadColor(ROOT.kGray)
        ROOT.gStyle.SetTitleColor(390, "a")
        ROOT.gStyle.SetFuncColor(ROOT.kOrange)

        ROOT.gStyle.SetGridColor(1)

        ROOT.gStyle.SetCanvasDefH(800)
        ROOT.gStyle.SetCanvasDefW(800)

        ROOT.gROOT.ForceStyle(1)
        self.debug("done!")

    ## get unique plot name
    # @param self "Me, myself and Irene"
    def __plotName(self, what=""):
        name = str(uuid.uuid4())
        if self.opts.useVarNameForPlotName:
            name = self.name
        return what + "_" + name + "." + self.opts.plot_format

    ## plot dispatcher
    # @param self "Me, myself and Irene"
    # @param node DOM XML element
    # @param mon monitored object
    # @param ref reference object
    # @param opts additional plot options
    def plot(self, node, mon=None, ref=None, opts=None):
        self.mon = self.ref = self.node = None

        self.debug("plot called for object of name " + node.getAttribute("name"))

        self.node = node
        self.mon = mon
        self.ref = ref

        self.plotOpts = []
        self.drawOpts = ""

        if opts != "":
            plotOpts = [opt.strip().lower() for opt in opts.split(";")]

            drawOpts = []
            for opt in plotOpts:
                if opt not in ["logx", "logy", "logz", "norm", "enorm", "box", "ratio"]:
                    drawOpts.append(opt)
                else:
                    self.plotOpts.append(opt)
                if opt == "enorm":
                    self.plotOpts.append("norm")

            if self.plotOpts:
                self.debug(
                    "additional plot options are: '%s'" % " ".join(self.plotOpts)
                )
            if drawOpts:
                self.drawOpts = " ".join(drawOpts)
                self.debug("additional draw options are: '%s'" % self.drawOpts)
                self.drawOpts = " " + self.drawOpts

        if not self.mon:
            self.error("monitored object not found, creation of plots skipped!")
            return "FAIL"

        self.name = self.mon.GetName()
        self.title = self.mon.GetTitle()

        if not self.ref and self.ref != False:
            self.warn("reference object not found, not all plots will be produced")

        self.className = mon.Class().GetName()

        status = "OK"
        if "TGraph" in self.className:
            status = self.__plotGraph()
        elif "TH1" in self.className:
            if "norm" in self.plotOpts:
                self.__normTH(self.mon)
                self.__normTH(self.ref)
            status = self.__plotTH1()
        elif "TH2" in self.className:
            if "norm" in self.plotOpts:
                self.__normTH(self.mon)
                self.__normTH(self.ref)
            status = self.__plotTH2()
        elif "TProfile" == self.className:
            if "norm" in self.plotOpts:
                self.__normTH(self.mon)
                self.__normTH(self.ref)
            status = self.__plotProf1D()
        elif "TProfile2D" == self.className:
            if "norm" in self.plotOpts:
                self.__normTH(self.mon)
                self.__normTH(self.ref)
            status = self.__plotProf2D()
        else:
            self.error(
                "unsupported object name=%s class=%s" % (self.name, self.className)
            )
            status = "FAIL"

        return status

    ## save TCanvas to png file
    # @param self "Me, myself and Irene"
    # @param type string with type attribute
    # @param plot plot file naem prefix
    # @return status string
    def __saveAs(self, canvas, type, plot):
        try:
            plotName = self.__plotName(plot)
            absPath = os.path.join(self.outputDir, plotName)
            canvas.Update()
            canvas.SaveAs(absPath)
            imgNode = self.xmldoc.createElement("img")
            imgNode.setAttribute("src", os.path.join("plots", plotName))
            imgNode.setAttribute("type", type)
            if os.path.exists(absPath):
                self.debug("plot '%s' has been created" % absPath)
                self.node.appendChild(imgNode)
                return "OK"
            else:
                self.warn("problem when saving plot '%s' to file" % absPath)
                return "WARN"
        except Exception:
            self.warn("unknown error when plotting %s for %s" % (type, self.name))

        return "WARN"

    ## plotter for TGraph, TGraphError, TGraphAsymmError etc.
    # @param self "Me, myself and Irene"
    def __plotGraph(self):
        status = ["OK"]
        self.debug("plotting %s of type %s" % (self.name, self.className))

        title = "%s;%s;%s" % (
            self.mon.GetTitle(),
            self.mon.GetXaxis().GetTitle(),
            self.mon.GetYaxis().GetTitle(),
        )
        graph = ROOT.TMultiGraph()

        canvas = self.__getCanvas()
        # with ratio pad
        if "ratio" in self.plotOpts:
            p1 = ROOT.TPad("p1", "", 0, 0, 1, 1)
            p2 = ROOT.TPad("p2", "", 0, 0, 1, 1)
            p1.SetBottomMargin(0.325)
            p2.SetTopMargin(0.675)
            p1.SetFillStyle(0)
            p2.SetFillStyle(0)
            p1.SetFillColor(ROOT.kWhite)
            p2.SetFillColor(ROOT.kWhite)
            p1.Draw()
            p2.Draw("same")
        else:
            p1 = canvas
            p2 = canvas
        p1.cd()

        xmin, xmax, ymin, ymax = self.__getMinMaxTGraph(self.mon, self.ref)

        dy = abs(ymax - ymin)
        y0 = ymin - dy * 0.1
        if ymin >= 0.0 and y0 < 0.0:
            y0 = 0.0
        ymin = y0
        ymax = ymax + dy * (self.unzoom - 1.0)

        p1.DrawFrame(xmin, ymin, xmax, ymax, title)

        ##      remove unneeded padding (10/7/2017)
        #       ROOT.gStyle.SetPadBottomMargin(0.2)

        self.mon.SetMarkerStyle(ROOT.kFullCircle)
        self.mon.SetMarkerColor(ROOT.kRed)
        self.mon.SetFillColor(ROOT.kRed)
        self.mon.SetFillStyle(3004)
        self.mon.SetLineColor(ROOT.kRed)

        if self.ref:
            self.ref.SetMarkerStyle(ROOT.kOpenCircle)
            self.ref.SetMarkerColor(ROOT.kBlue)
            self.ref.SetMarkerSize(1.0)
            self.ref.SetFillColor(ROOT.kBlue)
            self.ref.SetFillStyle(3005)
            self.ref.SetLineColor(ROOT.kBlue)

            graph.Add(self.ref, "P")

        graph.Add(self.mon, "P")

        graph.Draw("P" + self.drawOpts)

        legend = self.__legend()
        legend.Draw()
        pvaluePave = self.__pvaluePave()
        if pvaluePave:
            pvaluePave.Draw()

        titlePave = self.__titlePave()
        titlePave.Draw()

        configPave = self.__configPave(self.opts.supressReleaseLabel)
        if configPave:
            configPave.Draw()

        if "ratio" in self.plotOpts:
            if self.ref:

                def divop(y, r):
                    r, er = r[0], r[1:]
                    if not r:
                        return (None,) * len(y)
                    y, ey = y[0], y[1:]
                    d = y / r
                    dd = (
                        None
                        if dy is None
                        else -1.0
                        if dy < 0.0 or dr < 0.0
                        else d * math.sqrt((dy / y) ** 2 + (dr / r) ** 2)
                        if y
                        else 0.0
                        for dy, dr in zip(ey, er)
                    )
                    return (d,) + tuple(dd)

                p2.cd()
                self.rat = self.__makeDiffGraph(op=divop, name="rat")
                self.rat.SetFillStyle(0)
                self.rat.SetMarkerStyle(8)
                self.rat.SetMarkerSize(0.7)
                self.rat.SetMarkerColor(ROOT.kGray + 3)
                self.rat.SetLineColor(ROOT.kGray + 3)
                self.rat.GetYaxis().SetTitle("mon / ref")
                # self.rat.GetYaxis().SetRangeUser(0.0,2.0)
                self.rat.GetYaxis().SetNdivisions(202)
                self.rat.GetYaxis().SetTickLength(
                    self.rat.GetYaxis().GetTickLength() * 0.675 / 0.325
                )
                self.rat.GetXaxis().SetTitleOffset(1.2)
                p2.DrawFrame(xmin, 0.0, xmax, 2.0)
                self.rat.Draw("PE")
                ROOT.gPad.Update()
                line = ROOT.TLine(ROOT.gPad.GetUxmin(), 1.0, ROOT.gPad.GetUxmax(), 1.0)
                line.SetLineColor(ROOT.kGray - 2)
                line.SetLineStyle(ROOT.kDotted)
                line.Draw()

            p1.cd()
            if "logx" in self.plotOpts:
                ROOT.gPad.SetLogx(1)
            if "logy" in self.plotOpts:
                ROOT.gPad.SetLogy(1)

        status.append(self.__saveAs(canvas, "reg", "gr"))

        if all((self.ref, self.mon)):

            def subop(y, r):
                y, ey = y[0], y[1:]
                r, er = r[0], r[1:]
                d = y - r
                dd = (
                    None
                    if dy is None
                    else -1.0
                    if dy < 0.0 or dr < 0.0
                    else math.sqrt(dy**2 + dr**2)
                    for dy, dr in zip(ey, er)
                )
                return (d,) + tuple(dd)

            canvas.Clear()
            diffGraph = self.__makeDiffGraph(op=subop)

            diffGraph.SetMarkerColor(ROOT.kRed)
            xmin, xmax, ymin, ymax = self.__getMinMaxTGraph(diffGraph)

            dy = abs(ymax - ymin)
            y0 = ymin - dy * 0.1
            if ymin >= 0.0 and y0 < 0.0:
                y0 = 0.0
            ymax = ymax * self.unzoom
            if ymax == 0.0:
                ymax = ymin + (abs(ymin) * self.unzoom)

            ROOT.gPad.DrawFrame(xmin, y0, xmax, ymax, "diff %s" % title)

            if diffGraph.GetN():
                diffGraph.Draw("P" + self.drawOpts)

            titlePave = self.__titlePave("diff (mon - ref)")
            titlePave.Draw()

            configPave = self.__configPave()
            if configPave:
                configPave.Draw()

            status.append(self.__saveAs(canvas, "dif", "gd"))

        return self.__getStatus(status)

    ## get xmin, xmax, ymin, ymax for monitored and reference  TGraphs
    # @param self "Me, myself and Irene"
    def __getMinMaxTGraph(self, mon=None, ref=None):
        xmin = xmax = ymin = ymax = exmin = exmax = dxmin = None
        xd = ctypes.c_double(0)
        yd = ctypes.c_double(0)
        for g in (ref, mon):
            if g:
                x0 = None
                for i in range(g.GetN()):
                    if g.GetPoint(i, xd, yd) >= 0:
                        x = float(xd.value)
                        y = float(yd.value)
                    else:
                        continue

                    if x0 is not None:
                        dx = x - x0
                        if dx > 0.0 and (dxmin is None or dx < dxmin):
                            dxmin = dx
                    x0 = x

                    if self.className == "TGraph":
                        exh = exl = eyh = eyl = 0.0
                    else:
                        exh = max(0.0, g.GetErrorXhigh(i))
                        exl = max(0.0, g.GetErrorXlow(i))
                        eyh = max(0.0, g.GetErrorYhigh(i))
                        eyl = max(0.0, g.GetErrorYlow(i))

                    xh = x + exh
                    xl = x - exl
                    yh = y + eyh
                    yl = y - eyl

                    if xmin is None or xl < xmin:
                        xmin, exmin = xl, exl
                    if xmax is None or xh > xmax:
                        xmax, exmax = xh, exh
                    if ymin is None or yl < ymin:
                        ymin = yl
                    if ymax is None or yh > ymax:
                        ymax = yh

        # if no x-errors, add half-bin width
        if dxmin is not None:
            if exmin <= 0.0:
                xmin -= 0.5 * dxmin
            if exmax <= 0.0:
                xmax += 0.5 * dxmin

        # if no limits, then use graph range
        if ref:
            g = ref
        else:
            g = mon
        if g:
            if xmin is None or xmin >= xmax:
                xmin = g.GetHistogram().GetXaxis().GetXmin()
            if xmax is None or xmin >= xmax:
                xmax = g.GetHistogram().GetXaxis().GetXmax()
            if ymin is None or ymin >= ymax:
                ymin = g.GetHistogram().GetYaxis().GetXmin()
            if ymax is None or ymin >= ymax:
                ymax = g.GetHistogram().GetYaxis().GetXmax()

        return (xmin, xmax, ymin, ymax)

    ## create TGraph from the difference (or other binary op) between two graphs
    # @param self "Me, myself and Irene"
    def __makeDiffGraph(self, op, name="diff"):
        monN = self.mon.GetN()
        refN = self.ref.GetN()
        ypoints = [None] * monN
        if monN and refN:
            warnings.filterwarnings(
                "ignore", category=FutureWarning
            )  # ignore internal PyROOT "buffer.SetSize(N) is deprecated" warning in ROOT 6.20.
            monX = self.mon.GetX()
            monY = self.mon.GetY()
            refX = self.ref.GetX()
            refY = self.ref.GetY()
            if monX and monY and refX and refY:
                monEY = self.mon.GetEY()
                monEYlow = self.mon.GetEYlow()
                monEYhigh = self.mon.GetEYhigh()

                refEY = self.ref.GetEY()
                refEYlow = self.ref.GetEYlow()
                refEYhigh = self.ref.GetEYhigh()

                if not monEY:
                    monEY = [None] * monN
                if not monEYlow:
                    monEYlow = [None] * monN
                if not monEYhigh:
                    monEYhigh = [None] * monN
                points = {}
                for i, p in enumerate(zip(monX, monY, monEY, monEYlow, monEYhigh)):
                    x, y = p[0], p[1:]
                    points.setdefault(x, (i,) + y)

                if not refEY:
                    refEY = [None] * refN
                if not refEYlow:
                    refEYlow = [None] * refN
                if not refEYhigh:
                    refEYhigh = [None] * refN
                for r in zip(refX, refY, refEY, refEYlow, refEYhigh):
                    x, r = r[0], r[1:]
                    p = points.get(x)
                    if p is None:
                        continue
                    i, y = p[0], p[1:]
                    d = op(y, r)
                    if d[0] is None:
                        continue
                    ypoints[i] = d

            warnings.filterwarnings("default", category=FutureWarning)

        diffGraph = self.mon.Clone(self.mon.GetName() + "_" + name)
        diffGraph.SetTitle(self.mon.GetTitle() + " " + name)

        for i, p in reversed(list(enumerate(ypoints))):
            if p:
                y, ey, eylow, eyhigh = p
                diffGraph.SetPointY(i, y)
                if ey is not None:
                    diffGraph.SetPointEY(i, ey)
                if eylow is not None:
                    diffGraph.SetPointEYlow(i, eylow)
                if eyhigh is not None:
                    diffGraph.SetPointEYhigh(i, eyhigh)
            else:
                diffGraph.RemovePoint(i)

        return diffGraph

    def __normTH(self, obj=None):
        if obj:
            if "enorm" in self.plotOpts:
                self.debug(
                    "will normalize histogram '%s' to the number of entries"
                    % obj.GetName()
                )
                integral = obj.GetEntries()
            else:
                self.debug("will normalize histogram '%s'" % obj.GetName())
                integral = obj.Integral()
            if integral != 0.0:
                scale = 1.0 / integral
                self.debug("integral = %f, scale = %f" % (integral, scale))
                obj.Scale(scale)
            else:
                self.warn(
                    "cannot normalize histogram '%s', Integral = 0.0!" % obj.GetName()
                )

    ## plotter for TH1 histograms
    # @param self "Me, myself and Irene"
    def __plotTH1(self):
        status = ["OK"]
        self.debug("plotting %s of type %s" % (self.name, self.className))

        canvas = self.__getCanvas()
        # with ratio pad
        if "ratio" in self.plotOpts:
            p1 = ROOT.TPad("p1", "", 0, 0, 1, 1)
            p2 = ROOT.TPad("p2", "", 0, 0, 1, 1)
            p1.SetBottomMargin(0.325)
            p2.SetTopMargin(0.675)
            p1.SetFillStyle(0)
            p2.SetFillStyle(0)
            p1.SetFillColor(ROOT.kWhite)
            p2.SetFillColor(ROOT.kWhite)
            p1.Draw()
            p2.Draw("same")
            canvas.cd()
        else:
            p1 = canvas
            p2 = canvas

        titlePave = self.__titlePave()
        title = "%s;%s;%s" % (
            self.mon.GetTitle(),
            self.mon.GetXaxis().GetTitle(),
            self.mon.GetYaxis().GetTitle(),
        )
        p1.cd()
        stack = ROOT.THStack("dcube stack", title)

        self.mon.SetMarkerColor(ROOT.kRed)

        self.mon.SetLineColor(ROOT.kRed)
        self.mon.SetFillStyle(3004)
        self.mon.SetLineColor(ROOT.kRed)

        ymax = self.mon.GetMaximum()
        ymin = self.mon.GetMinimum()

        if self.ref:
            self.ref.SetMarkerStyle(ROOT.kOpenCircle)
            self.ref.SetMarkerColor(ROOT.kBlue)
            self.ref.SetMarkerSize(1.0)

            self.ref.SetFillColor(ROOT.kBlue)
            self.ref.SetLineColor(ROOT.kBlue)
            self.ref.SetFillStyle(3005)

            stack.Add(self.ref)

            if ymax < self.ref.GetMaximum():
                ymax = self.ref.GetMaximum()
            if ymin > self.ref.GetMinimum():
                ymin = self.ref.GetMinimum()

        stack.Add(self.mon)

        # yspread = abs( ymax - ymin )
        # self.panic( "ymin, ymax, yspread %f, %f, %f" % ( ymin, ymax, yspread) )
        # ymax = ymin + ( self.unzoom * yspread )
        # self.panic( "ymax %f" % ymax )

        ymax = ymax * self.unzoom
        if ymax == 0.0:
            ymax = ymin + (abs(ymin) * self.unzoom)
        if "ratio" in self.plotOpts:
            ymax *= 1.2 if "logy" not in self.plotOpts else 50
        stack.SetMaximum(ymax)

        config = self.__configPave(self.opts.supressReleaseLabel)
        legend = self.__legend()

        p1.cd()
        stack.Draw("NOSTACK" + self.drawOpts)
        canvas.cd()
        titlePave.Draw()
        if config:
            config.Draw()
        legend.Draw()
        pvaluePave = self.__pvaluePave()
        if pvaluePave:
            pvaluePave.Draw()

        if "ratio" in self.plotOpts:
            if self.ref:
                p2.cd()
                self.rat = self.mon.Clone(self.mon.GetName() + "_rat")
                if not self.rat.GetSumw2N():
                    self.rat.Sumw2()
                if not self.ref.GetSumw2N():
                    ref = self.ref.Clone(self.ref.GetName() + "_ratref")
                    ref.Sumw2()
                else:
                    ref = self.ref
                self.rat.Divide(self.rat, ref)
                self.rat.SetFillStyle(0)
                self.rat.SetMarkerStyle(8)
                self.rat.SetMarkerSize(0.7)
                self.rat.SetMarkerColor(ROOT.kGray + 3)
                self.rat.SetLineColor(ROOT.kGray + 3)
                self.rat.GetYaxis().SetTitle("mon / ref")
                self.rat.GetYaxis().SetRangeUser(0.0, 2.0)
                self.rat.GetYaxis().SetNdivisions(202)
                self.rat.GetYaxis().SetTickLength(
                    self.rat.GetYaxis().GetTickLength() * 0.675 / 0.325
                )
                self.rat.GetXaxis().SetTitleOffset(1.2)
                self.rat.Draw("PE")
                ROOT.gPad.Update()
                line = ROOT.TLine(ROOT.gPad.GetUxmin(), 1.0, ROOT.gPad.GetUxmax(), 1.0)
                line.SetLineColor(ROOT.kGray - 2)
                line.SetLineStyle(ROOT.kDotted)
                line.Draw()

                p1.cd()
                stack.GetXaxis().SetLabelSize(0.0001)
            else:
                p1.cd()
            if "logx" in self.plotOpts:
                ROOT.gPad.SetLogx(1)
            if "logy" in self.plotOpts:
                ROOT.gPad.SetLogy(1)

        status.append(self.__saveAs(canvas, "reg", "h1r"))

        canvas = self.__getCanvas()
        diffHist = self.mon.Clone()
        diffHist.Reset()
        diffHist.SetTitle("diff %s" % title)
        diffHist.SetLineColor(ROOT.kRed)

        if self.ref:
            diffHist.Add(self.ref, self.mon, 1.0, -1.0)
        else:
            diffHist.Add(self.mon, self.mon, 1.0, -1.0)

        canvas.Clear()
        canvas.cd()

        titlePave = self.__titlePave("diff")
        configPave = self.__configPave()

        ymax = diffHist.GetMaximum()
        ymin = diffHist.GetMinimum()

        ymax = ymax * self.unzoom
        if ymax == 0.0:
            ymax = ymin + (abs(ymin) * self.unzoom)

        if ymax:
            diffHist.SetMaximum(ymax)

        diffHist.Draw(self.drawOpts)
        titlePave.Draw()
        if configPave:
            configPave.Draw()

        status.append(self.__saveAs(canvas, "dif", "h1d"))

        return self.__getStatus(status)

    ## plotter for TH2 histograms
    # @param self "Me, myself and Irene"
    def __plotTH2(self, typ="h2"):
        status = ["OK"]
        self.debug("plotting %s of type %s" % (self.name, self.className))

        canvas = self.__getCanvas()
        title = "%s;%s;%s;%s" % (
            self.mon.GetTitle(),
            self.mon.GetXaxis().GetTitle(),
            self.mon.GetYaxis().GetTitle(),
            self.mon.GetZaxis().GetTitle(),
        )

        configPave = self.__configPave(self.opts.supressReleaseLabel)
        titlePave = self.__titlePave()
        pvaluePave = self.__pvaluePave()

        if self.ref:
            bottom = self.mon.Clone()
            top = self.mon.Clone()
            same = self.mon.Clone()

            bottom.Reset()
            bottom.SetLineColor(ROOT.kBlack)
            bottom.SetFillColor(ROOT.kBlue)
            bottom.SetLineWidth(2)
            top.Reset()
            top.SetLineColor(ROOT.kBlack)
            top.SetFillColor(ROOT.kRed)
            top.SetLineWidth(2)
            same.Reset()
            same.SetLineColor(ROOT.kBlack)
            same.SetFillColor(ROOT.kGreen)
            same.SetLineWidth(2)

            for i in range(self.ref.GetNbinsX() + 1):
                for j in range(self.ref.GetNbinsY() + 1):
                    refBin = self.ref.GetBinContent(i, j)
                    monBin = self.mon.GetBinContent(i, j)

                    if refBin < monBin:
                        top.SetBinContent(i, j, monBin - refBin)
                        bottom.SetBinContent(i, j, refBin)
                    elif refBin > monBin:
                        bottom.SetBinContent(i, j, refBin - monBin)
                        top.SetBinContent(i, j, monBin)
                    else:
                        same.SetBinContent(i, j, refBin)

            stack = ROOT.THStack("dcube stack", title)
            stack.Add(same)
            stack.Add(bottom)
            stack.Add(top)
            # stack.UseCurrentStyle()

            Y1NDC = self.configPave.GetY1NDC() if self.configPave else 0.9
            legend = ROOT.TLegend(0.1, Y1NDC, 0.45, Y1NDC - 0.08, "", "NDC")
            legend.SetTextFont(102)
            legend.SetTextSize(0.02)
            legend.SetTextColor(1)
            legend.SetBorderSize(1)

            legend.AddEntry(top, self.opts.monitoredName, "F")
            legend.AddEntry(bottom, self.opts.referenceName, "F")
            legend.AddEntry(same, "same", "F")

            canvas.cd()
            if "box" in self.plotOpts:
                stack.Draw("box nostack" + self.drawOpts)
            else:
                stack.Draw("lego1 nostack" + self.drawOpts)
            titlePave.Draw()
            if configPave:
                configPave.Draw()
            legend.Draw()

            if pvaluePave:
                pvaluePave.Draw()

            canvas.Draw()

        else:
            canvas.cd()
            self.mon.Draw(self.drawOpts)

            if configPave:
                configPave.Draw()
            titlePave.Draw()
            if pvaluePave:
                pvaluePave.Draw()
            canvas.Draw()

        status.append(self.__saveAs(canvas, "reg", typ + "r"))

        # new configPave in case we supressReleaseLabel before
        configPave = self.__configPave()

        # make diff plot
        canvas = self.__getCanvas()
        diffHist = self.mon.Clone()
        diffHist.Reset()
        diffHist.SetFillColor(ROOT.kRed)
        diffHist.SetLineColor(ROOT.kBlack)

        if self.ref:
            diffHist.Add(self.ref, self.mon, 1.0, -1.0)
        else:
            diffHist.Add(self.mon, self.mon, 1.0, -1.0)

        canvas.Clear()
        if "box" in self.plotOpts:
            diffHist.Draw("colz1 0" + self.drawOpts)
        else:
            diffHist.Draw("LEGO1 0" + self.drawOpts)
        titlePave = self.__titlePave("diff")
        titlePave.Draw()
        if configPave:
            configPave.Draw()
        if pvaluePave:
            pvaluePave.Draw()
        status.append(self.__saveAs(canvas, "dif", typ + "d"))

        # make projection x plot
        canvas = self.__getCanvas()
        canvas.Clear()
        projX = ROOT.THStack("projectionX", "projectionX")

        monPX = self.mon.ProjectionX("mon px " + str(self.name))

        if "norm" in self.plotOpts:
            self.__normTH(monPX)

        monPX.SetFillStyle(3004)
        monPX.SetLineColor(ROOT.kRed)
        projX.Add(monPX)

        ymax = monPX.GetMaximum()

        if self.ref:
            refPX = self.ref.ProjectionX("ref px " + str(self.name))
            if "norm" in self.plotOpts:
                self.__normTH(refPX)
            refPX.SetFillStyle(3005)
            refPX.SetFillColor(ROOT.kBlue)
            refPX.SetLineColor(ROOT.kBlue)
            if ymax < refPX.GetMaximum():
                ymax = refPX.GetMaximum()

            projX.Add(refPX)

        projX.SetMaximum(ymax * self.unzoom)

        canvas.cd()
        if "logx" in self.plotOpts:
            ROOT.gPad.SetLogx(1)
        if "logy" in self.plotOpts:
            ROOT.gPad.SetLogy(1)
        if "logz" in self.plotOpts:
            ROOT.gPad.SetLogz(1)

        projX.Draw("NOSTACK" + self.drawOpts)

        titlePave = self.__titlePave("proj X")
        titlePave.Draw()
        if pvaluePave:
            pvaluePave.Draw()
        if configPave:
            configPave.Draw()

        Y1NDC = self.configPave.GetY1NDC() if self.configPave else 0.9
        legend = ROOT.TLegend(0.1, Y1NDC, 0.45, Y1NDC - 0.06, "", "NDC")
        legend.SetTextFont(102)
        legend.SetTextSize(0.02)
        legend.SetTextColor(1)
        legend.SetBorderSize(1)

        legend.AddEntry(monPX, "mon projection X", "F")
        if self.ref:
            legend.AddEntry(refPX, "ref projection X", "F")
        legend.Draw()

        status.append(self.__saveAs(canvas, "prx", typ + "x"))

        # make projection y plot
        canvas = self.__getCanvas()
        canvas.cd()
        if "logx" in self.plotOpts:
            ROOT.gPad.SetLogx(1)
        if "logy" in self.plotOpts:
            ROOT.gPad.SetLogy(1)
        if "logz" in self.plotOpts:
            ROOT.gPad.SetLogz(1)
        canvas.Clear()

        projY = ROOT.THStack("projectionY", "projectionY")
        monPY = self.mon.ProjectionY("mon py " + str(self.name))
        if "norm" in self.plotOpts:
            self.__normTH(monPY)
        monPY.SetFillStyle(3004)
        monPY.SetLineColor(ROOT.kRed)
        projY.Add(monPY)

        ymax = monPY.GetMaximum()

        if self.ref:
            refPY = self.ref.ProjectionY("ref py " + str(self.name))
            if "norm" in self.plotOpts:
                self.__normTH(refPY)
            refPY.SetFillStyle(3005)
            refPY.SetFillColor(ROOT.kBlue)
            refPY.SetLineColor(ROOT.kBlue)
            if ymax < refPY.GetMaximum():
                ymax = refPY.GetMaximum()

            projY.Add(refPY)

        projY.SetMaximum(ymax * self.unzoom)

        canvas.cd()

        projY.Draw("NOSTACK" + self.drawOpts)

        titlePave = self.__titlePave("proj Y")
        titlePave.Draw()
        if configPave:
            configPave.Draw()
        if pvaluePave:
            pvaluePave.Draw()

        Y1NDC = self.configPave.GetY1NDC() if self.configPave else 0.9
        legend = ROOT.TLegend(0.1, Y1NDC, 0.45, Y1NDC - 0.06, "", "NDC")
        legend.SetTextFont(102)
        legend.SetTextSize(0.02)
        legend.SetTextColor(1)
        legend.SetBorderSize(1)

        legend.AddEntry(monPY, "mon projection Y", "F")

        if self.ref:
            legend.AddEntry(refPY, "ref projection Y", "F")
        legend.Draw()

        status.append(self.__saveAs(canvas, "pry", typ + "y"))

        return self.__getStatus(status)

    ## plotter for 1D TProfile
    # @param self "Me, myself and Irene"
    def __plotProf1D(self):
        status = []
        self.debug("plotting %s of type %s" % (self.name, self.className))

        canvas = self.__getCanvas()
        # with ratio pad
        if "ratio" in self.plotOpts:
            p1 = ROOT.TPad("p1", "", 0, 0, 1, 1)
            p2 = ROOT.TPad("p2", "", 0, 0, 1, 1)
            p1.SetBottomMargin(0.325)
            p2.SetTopMargin(0.675)
            p1.SetFillStyle(0)
            p2.SetFillStyle(0)
            p1.SetFillColor(ROOT.kWhite)
            p2.SetFillColor(ROOT.kWhite)
            p1.Draw()
            p2.Draw("same")
            p1.cd()
        else:
            p1 = canvas
            p2 = canvas

        titlePave = self.__titlePave()
        configPave = self.__configPave(self.opts.supressReleaseLabel)
        legend = self.__legend()

        self.mon.SetMarkerColor(ROOT.kRed)
        self.mon.SetMarkerSize(0.9)
        self.mon.SetFillColor(ROOT.kRed)
        self.mon.SetFillStyle(3005)
        self.mon.SetLineColor(ROOT.kRed)

        ymax = self.mon.GetMaximum()
        ymin = self.mon.GetMinimum()
        if self.ref:
            self.ref.SetMarkerStyle(ROOT.kOpenCircle)
            self.ref.SetMarkerColor(ROOT.kBlue)
            self.ref.SetMarkerSize(1.0)
            self.ref.SetFillColor(ROOT.kBlue)
            self.ref.SetFillStyle(3004)
            self.ref.SetLineColor(ROOT.kBlue)

            if ymax < self.ref.GetMaximum():
                ymax = self.ref.GetMaximum()
            if ymin > self.ref.GetMinimum():
                ymin = self.ref.GetMinimum()

        # yspread = abs( ymax)  + abs( ymin )
        # ymax = ymin + ( self.unzoom * yspread )
        ymax = ymax * self.unzoom
        if ymax == 0.0:
            ymax = ymin + (abs(ymin) * self.unzoom)
        if "ratio" in self.plotOpts:
            ymax *= 1.2 if "logy" not in self.plotOpts else 50

        self.mon.SetMaximum(ymax)
        if self.ref:
            self.ref.SetMaximum(ymax)

        if self.ref:
            self.ref.Draw(self.drawOpts)
        self.mon.Draw("SAME" + self.drawOpts)

        titlePave.Draw()
        if configPave:
            configPave.Draw()
        legend.Draw()
        pvaluePave = self.__pvaluePave()
        if pvaluePave:
            pvaluePave.Draw()

        if "ratio" in self.plotOpts:
            if self.ref:
                p2.cd()
                self.rat = self.mon.ProjectionX(self.mon.GetName() + "_monratioproj")
                if not self.rat.GetSumw2N():
                    self.rat.Sumw2()
                refX = self.ref.ProjectionX(self.mon.GetName() + "_refratioproj")
                if not refX.GetSumw2N():
                    refX.Sumw2()
                self.rat.Divide(refX)
                self.rat.SetFillStyle(0)
                self.rat.SetMarkerStyle(8)
                self.rat.SetMarkerSize(0.7)
                self.rat.SetMarkerColor(ROOT.kGray + 3)
                self.rat.SetLineColor(ROOT.kGray + 3)
                self.rat.GetYaxis().SetTitle("mon / ref")
                self.rat.GetYaxis().SetRangeUser(0.0, 2.0)
                self.rat.GetYaxis().SetNdivisions(202)
                self.rat.GetYaxis().SetTickLength(
                    self.rat.GetYaxis().GetTickLength() * 0.675 / 0.325
                )
                self.rat.GetXaxis().SetTitleOffset(1.2)
                self.rat.Draw("PE")
                ROOT.gPad.Update()
                line = ROOT.TLine(ROOT.gPad.GetUxmin(), 1.0, ROOT.gPad.GetUxmax(), 1.0)
                line.SetLineColor(ROOT.kGray - 2)
                line.SetLineStyle(ROOT.kDotted)
                line.Draw()

                p1.cd()

            p1.cd()
            if "logx" in self.plotOpts:
                ROOT.gPad.SetLogx(1)
            if "logy" in self.plotOpts:
                ROOT.gPad.SetLogy(1)

        status.append(self.__saveAs(canvas, "reg", "p1r"))

        canvas = self.__getCanvas()
        canvas.Clear()
        canvas.cd()

        # result of Add() can't be a TProfile, so make a suitable histogram
        diffProfile = self.mon.ProjectionX(self.mon.GetName() + "_diffproj")
        diffProfile.SetDirectory(0)
        diffProfile.Reset()
        if self.ref:
            diffProfile.Add(self.ref, self.mon, 1.0, -1.0)
        else:
            diffProfile.Add(self.mon, self.mon, 1.0, -1.0)

        titlePave = self.__titlePave("diff")

        ymin = diffProfile.GetMinimum()
        ymax = diffProfile.GetMaximum()

        ymax = ymax * self.unzoom
        if ymax == 0.0:
            ymax = ymin + (abs(ymin) * self.unzoom)

        diffProfile.SetMaximum(ymax)

        configPave = self.__configPave()
        diffProfile.Draw(self.drawOpts)
        titlePave.Draw()
        if configPave:
            configPave.Draw()

        status.append(self.__saveAs(canvas, "dif", "p1d"))

        return self.__getStatus(status)

    ## plotter for TProfile2D
    # @param self "Me, myself and Irene"
    def __plotProf2D(self):
        # convert to TH2D and plot using __plotTH2()
        self.mon = self.mon.ProjectionXY(name=self.name + "_mon_pxy", option="W")
        if self.ref:
            self.ref = self.ref.ProjectionXY(name=self.name + "_ref_pxy", option="W")
        return self.__plotTH2("p2")

    ## get status string from a list of strings
    # @param self "Me, myself and Irene"
    # @param sl list of strings
    def __getStatus(self, sl):
        if "FAIL" in sl:
            return "FAIL"
        if "WARN" in sl:
            return "WARN"
        return "OK"

    def __configPave_AddText(self, s1, s2, width=31):
        if s2 == "" or s2 == "*":
            return 0
        if len(s1) + len(s2) > width:
            s1 = s1.rstrip().ljust(width - len(s2))
        self.configPave.AddText(s1 + s2)
        return 1

    ## runtime configuration pave
    # @param self "Me, myself and Irene"
    def __configPave(self, supressReleaseLabel=False):
        self.configPave = ROOT.TPaveText()
        self.configPave.SetMargin(0.02)
        self.configPave.SetBorderSize(1)
        self.configPave.SetTextColor(1)
        self.configPave.SetTextSize(0.02)
        self.configPave.SetTextFont(102)
        self.configPave.SetTextAlign(12)
        lines = 0
        lines += self.__configPave_AddText("branch:  ", self.opts.branch)
        if not supressReleaseLabel:
            lines += self.__configPave_AddText("release: ", self.opts.install)
        lines += self.__configPave_AddText("cmt:     ", self.opts.cmtconfig)
        lines += self.__configPave_AddText("project: ", self.opts.project)
        lines += self.__configPave_AddText("jobId:   ", self.opts.jobId)
        if lines:
            self.configPave.SetX1NDC(0.1)
            self.configPave.SetY1NDC(0.9 - (lines * 0.020))
            self.configPave.SetX2NDC(0.45)
            self.configPave.SetY2NDC(0.9)
        else:
            self.configPave = None
        return self.configPave

    ## p-value pave
    # @param self "Me, myself and Irene"
    def __pvaluePave(self):
        pvaluePave = ROOT.TPaveText()
        pvaluePave.SetBorderSize(1)
        pvaluePave.SetTextColor(1)
        pvaluePave.SetTextSize(0.02)
        pvaluePave.SetTextFont(42)
        pvaluePave.SetTextAlign(12)

        lines = 0
        pvalues = self.node.getElementsByTagName("pvalue")
        for pvalue in pvalues:
            test = pvalue.getAttribute("test")
            status = pvalue.getAttribute("status")
            pval = self.__getCData(pvalue.childNodes)
            text = "p-value = %s" % pval

            if test == "chi2":
                text = "#chi^{2}     %s" % text
            elif test == "meanY":
                text = "<y>  %s" % text
            elif test == "KS":
                testopt = pvalue.getAttribute("testopt")
                if testopt and testopt == "norm":
                    text = "KSn  %s" % text
                else:
                    text = "KS   %s" % text
            else:
                text = "%-3s  %s" % (test, text)

            text = pvaluePave.AddText(text)
            if status == "FAIL":
                text.SetTextColor(ROOT.kRed)
            if status == "WARN":
                text.SetTextColor(ROOT.kOrange)
            lines += 1

        if lines:
            pvaluePave.SetX1NDC(0.715)
            pvaluePave.SetX2NDC(0.9)
            pvaluePave.SetY1NDC(0.9)
            pvaluePave.SetY2NDC(0.9 - (lines * 0.021))
            return pvaluePave

    ## title pave
    # @param self "Me, myself and Irene"
    def __titlePave(self, what=""):
        titlePave = ROOT.TPaveText(0.0, 1.0, 1.0, 0.93, "NDC")
        titlePave.SetBorderSize(0)
        titlePave.SetTextColor(ROOT.kBlack)
        titlePave.SetTextSize(0.028)
        titlePave.SetTextFont(42)
        titlePave.SetFillColor(ROOT.kGray)
        titlePave.SetTextAlign(12)
        titlePave.AddText("title: %s" % self.title)
        # titlePave.AddText( "name: %s" % self.name )
        if what == "":
            what = "normal"
        titlePave.AddText("%s plot" % what)
        return titlePave

    ## plot legend
    # @param self "Me, myself and Irene"
    def __legend(self):
        Y1NDC = self.configPave.GetY1NDC() if self.configPave else 0.9
        legend = ROOT.TLegend(0.1, Y1NDC, 0.45, Y1NDC - 0.05, "", "NDC")
        legend.SetTextFont(42)
        legend.SetTextSize(0.02)
        legend.SetTextColor(ROOT.kBlack)
        legend.SetBorderSize(1)

        if "TH" in self.className:
            legend.AddEntry(self.mon, self.opts.monitoredName)  # "f" )
            if self.ref:
                legend.AddEntry(self.ref, self.opts.referenceName)  # , "f" )
        else:
            legend.AddEntry(self.mon, self.opts.monitoredName, "pe")
            if self.ref:
                legend.AddEntry(self.ref, self.opts.referenceName, "pe")

        return legend

    ## CDATA getter
    # @param self "Me, myself and Irene"
    # @param nodeList list of DOM nodes
    def __getCData(self, nodeList):
        cdata = ""
        for node in nodeList:
            if node.nodeType == node.TEXT_NODE:
                cdata += node.data
        return cdata


##
# @class test_DCubePlotter
# @author Krzysztof Daniel Ciba (Krzysztof.Ciba@NOSPAMgmail.com)
# @brief test case for DCubePlotter
class test_DCubePlotter(unittest.TestCase):
    ## test case setup
    # @param self "Me, myself and Irene"
    def setUp(self):
        pass

    ## c'tor
    # @param self "Me, myself and Irene"
    def test_01_ctor(self):
        try:
            self.plotter = DCubePlotter()
        except Exception:
            pass
        self.assertEqual(isinstance(self.plotter, DCubePlotter), True)

    ## plot diff objects
    # @param self "Me, myself and Irene"
    def test_02_plot(self):
        pass


# test suite execution
if __name__ == "__main__":
    testLoader = unittest.TestLoader()
    suite = testLoader.loadTestsFromTestCase(test_DCubePlotter)
    unittest.TextTestRunner(verbosity=3).run(suite)
