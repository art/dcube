#!/bin/env python

# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

import sys
import os

if __name__ == "__main__":
    ## to run ROOT in batch mode
    sys.argv.insert(1, "-b")

    sys.path = [os.path.dirname(os.path.dirname(os.path.abspath(__file__)))] + sys.path

    try:
        # this `python` import prefix is sub-optimal, but allows us to
        # dispatch into the module, where we can then use relative imports.
        from python.DCubeApp import DCubeApp
    except ImportError as import_error:
        try:
            from DCubeClient.DCubeApp import DCubeApp
        except ImportError:
            raise import_error

    DCubeApp()
